# Aurea App iOS

The AUREA module is a tool that was developed as part of the City of Kassel's Smart City concept 
(Smart Kassel | https://www.kassel.de/smart) in collaboration with the University of Kassel's 
Communication technology department (Universität Kassel | www.uni-kassel.de). 

The module enables citizens to experience the city in a completely new way. It is a building block that can be integrated 
into larger applications (such as city apps) or developed into a stand-alone AR application.

This software is the Open-Source part (Software 2) of the Aurea App for iOS.  
It uses  the copyright protected Framework (Software 1) for iOS (Provided by [ComTec](https://www.comtec.eecs.uni-kassel.de/)).  

To get a licenced copy of the Framework please contact the following Mail Address: comtec@uni-kassel.de  

The copyright protected Framework (Software 1) may only be used if a licence has previously been granted by the University of Kassel.


## Requirements

iOS Version: `>16`  
xCode Version: `>15.1`  
Framework (Software 1) for iOS in Version `v1.0` (Provided by [ComTec](https://www.comtec.eecs.uni-kassel.de/)).

## Development Setup

1. Clone this Repository
2. Copy the required XCFramework `.xcframework` into the Folder "Aurea/Frameworks".
3. Open the Project in xCode
4. Wait for xCode finishing downloading Dependencies
5. Build and run the App

## Licence

This software is licensed under the [GNU LESSER GENERAL PUBLIC LICENSE V3](https://www.gnu.org/licenses/lgpl-3.0.en.html). 


## Project Structure

| Package       | Description |
| -             | -           |
| Main          | Files and Components of the Main View. |
| Repo          | Frost-API Implementation and Response Models. |
| AR            | Files and Components of the AR-Feature. |
| Compass       | Files and Components of the Compass-Feature. |
| Settings      | Files and Components of the Settings View. |
| Contact       | Files and Components of the Contact View. |


## Third Party Libraries
 
| Name                  | Description   |
| -                     | -             |
| Alamofire             | Alamofire is an HTTP networking library written in Swift. <br>https://github.com/Alamofire/Alamofire |
| Introspect            | Introspect allows you to get the underlying UIKit or AppKit element of a SwiftUI view. <br>https://github.com/siteline/swiftui-introspect |
| Lottie                | Lottie is a cross-platform library, that natively renders vector-based animations and art in realtime with minimal code. <br>https://github.com/airbnb/lottie-ios/ |
| PermissionsSwiftUI    | Displays and handles permissions in SwiftUI. <br>https://github.com/jevonmao/PermissionsSwiftUI |
| swift-collections     | Package of data structure implementations for the Swift programming language. <br>https://github.com/apple/swift-collections |
| Font Awesome Free | Font Awesome is the Internet's icon library and toolkit, used by millions of designers, developers, and content creators. <br> https://github.com/FortAwesome/Font-Awesome

## Workflow

* Reporting bugs:  
If you come across any issues while using the Aurea App, please report them by creating a new issue on the GitLab repository.

* Reporting bugs form:  
    ```
    App version:    1.3.0
    iOS version:    17.1.2
    iPhone model:   iPhone 15
    Description:    ...
    ```

* Providing feedback:   
If you have any feedback or suggestions for the Aurea App project, please let us know by creating a new issue or by sending an email to the project maintainer.

