//
//  LottieView.swift
//  CityPad
//
//  Created by lichen on 06.10.22.
//

import SwiftUI
import Lottie

/// Loads a Lottie animation from a file and plays it.
/// @property filename: name of the file to load
/// @property repeating: number of times to repeat the animation
struct LottieView: UIViewRepresentable {
    typealias UIViewType = UIView
    var filename : String
    var repeating : Float = 1.0
    
    private var animationView: AnimationView
    
    init(filename: String, repeating: Float) {
        self.filename = filename
        self.repeating = repeating
        
        self.animationView = AnimationView()
         animationView.animation = Animation.named (filename)
         animationView.contentMode =  .scaleAspectFit
    }
    
    func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
        let view = UIView(frame: .zero)
        animationView.play(fromProgress: 0, //Start
                           toProgress: 1, //End
                            loopMode: LottieLoopMode.repeat(repeating))
        animationView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(animationView)
        
        NSLayoutConstraint.activate([
            animationView.widthAnchor.constraint(equalTo:view.widthAnchor),
            animationView.heightAnchor.constraint(equalTo:view.heightAnchor)
        ])
       
        return view
    }
    
    func updateUIView( _ uiView: UIView, context: UIViewRepresentableContext<LottieView>){}
}

struct NearbyLottiePreview: PreviewProvider {
    static var previews: some View {
        GeometryReader {geo in
            ZStack {
                CompassView(rotation: 0)
                
                CompassHomeItemView(x: geo.size.width / 2, y: geo.size.height / 2)
                
                CompassCurrentRingView(currentRing: .nearby, width: geo.size.width / 2)
                
                LottieView(filename: "nearby", repeating: .infinity)
                    .clipShape(Circle())
                    .frame(width: 50)
            }
        }
    }
}
