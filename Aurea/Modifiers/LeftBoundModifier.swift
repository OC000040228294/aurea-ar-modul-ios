//
//  LeftBoundModifier.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct LeftBoundModifier: ViewModifier {
    func body(content: Content) -> some View {
        HStack {
            content
            Spacer()
        }
    }
}
