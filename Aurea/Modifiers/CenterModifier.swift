//
//  CenterModifier.swift
//  CityPad
//
//  Created by lichen on 21.10.22.
//

import SwiftUI

struct CenterBoundModifier: ViewModifier {
    func body(content: Content) -> some View {
        HStack {
            Spacer()
            content
            Spacer()
        }
    }
}
