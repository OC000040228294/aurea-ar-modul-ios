//
//  BottomBoundModifier.swift
//  CityPad
//
//  Created by lichen on 28.10.22.
//

import SwiftUI

struct BottomBoundModifier: ViewModifier {
    func body(content: Content) -> some View {
        VStack {
            Spacer()
            content
        }
    }
}
