//
//  CenterScreenModifier.swift
//  CityPad
//
//  Created by lichen on 06.12.22.
//

import SwiftUI

struct CenterScreenModifier: ViewModifier {
    func body(content: Content) -> some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                content
                Spacer()
            }
            Spacer()
        }
    }
}
