//
//  CardModifier.swift
//  CityPad
//
//  Created by lichen on 05.02.23.
//

import SwiftUI

struct CardModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .cornerRadius(5)
            .shadow(color: .black.opacity(0.4), radius: 2.0)
    }
}
