//
//  TopBoundModifier.swift
//  CityPad
//
//  Created by lichen on 21.11.22.
//

import SwiftUI

struct TopBoundModifier: ViewModifier {
    func body(content: Content) -> some View {
        VStack {
            content
            Spacer()
        }
    }
}
