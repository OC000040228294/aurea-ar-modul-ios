//
//  ContactHome.swift
//  Aurea
//
//  Created by lichen on 07.06.23.
//

import SwiftUI

/// The Contact View 
/// 
///
/// Displays the contact information of the app
struct ContactHome: View {
    @State private var toggled = false
    
    var body: some View {
        ScrollView {
            VStack {
                HStack(alignment: .center) {
                    Image(.logoSmartkassel)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 200)
                        .padding(16)
                }
                
                VStack(alignment: .leading, spacing: 16) {
                    Text("Hast Du Fragen oder eigene Ideen zu AUREA?\nWende Dich gerne an unser Projektbüro.")
                        .multilineTextAlignment(.leading)
                        .foregroundColor(.black)
                        .padding(.top, 16)
                    
                    VStack(alignment: .leading, spacing: 16) {
                        HStack(alignment: .center, spacing: 8) {
                            Image(systemName: "envelope")
                                .foregroundColor(.black)
                                .frame(width: 16, height: 16)
                            Link("smart@kassel.de", destination: URL(string: "mailto:smart@kassel.de")!)
                        }
                        
                        HStack(alignment: .center, spacing: 8) {
                            Image(systemName: "arrow.up.right")
                                .foregroundColor(.black)
                                .frame(width: 16, height: 16)
                            Link("Smart Kassel", destination: URL(string: "https://www.kassel.de/einrichtungen/aurea/index.php")!)
                        }
                    }
                    .padding(.leading, 32)
                    
                    Text("Unser Technologiepartner:")
                        .font(.title2)
                        .foregroundColor(.black)
                        .fontWeight(.bold)
                        .padding(.top, 16)
                    
                    Image(.logoKvv)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 250, height: 80)
                        .padding(.horizontal, 32)
                    
                    Text("Die App ist eine Entwicklung von")
                        .foregroundColor(.black)
                    
                    Image(.logoUniKasselComtec)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 230)
                        .onTapGesture(count: 5, perform: {
                            toggled = true
                        })
                        .padding(.horizontal, 32)
                        .sheet(isPresented: $toggled) {
                            VStack {
                                Text("Entwickelt von Lars Mathuseck, Sebastian Lange und Ludwig Chen Li.")
                                    .padding()
                            }
                        }
                        .padding(.top, 16)
                    
                    Text("Unsere Förderer:")
                        .font(.title2)
                        .foregroundColor(.black)
                        .fontWeight(.bold)
                        .padding(.top, 32)
                    
                    Image(.logoMinisterien)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 280)
                        .padding(.horizontal, 20)
                }
            }
            .padding(.horizontal)
        }
        .background(.white)
        
        
    }
}

struct ContactHome_Previews: PreviewProvider {
    static var previews: some View {
        ContactHome()
    }
}
