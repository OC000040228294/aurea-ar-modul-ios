//
//  ARHoverView.swift
//  CityPad
//
//  Created by lichen on 24.04.23.
//

import SwiftUI

/// View for the hover view of a sensor
/// It is used to display the sensor data in the AR view
struct ARHoverView: View {
    @AppStorage("opacity") var opacity = hoverOpacity
    
    var sensor: any Sensor
    var current: Int
    var max: Int
    var maxOpacity = false
    
    var body: some View {
        ZStack {
            switch sensor.type {
            case .PLANT:
                if let plantSensor = sensor as? PlantSensor {
                    PlantSensorView(name: sensor.name, data: plantSensor.plantData, distance: Int(sensor.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .TRAFFIC:
                if let trafficSensor = sensor as? TrafficSensor {
                    TrafficSensorView(data: trafficSensor.trafficData, name: trafficSensor.name, distance: Int(trafficSensor.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .SWIMMING:
                if let swimming = sensor as? SwimmingPoolSensor {
                    SwimmingPoolSensorView(data: swimming.swimmingPoolData, distance: Int(swimming.distance), name: swimming.name, current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .PARKING:
                if let parking = sensor as? ParkingSensor {
                    ParkingSensorView(data: parking.parkingData, name: parking.name, distance: Int(parking.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .LANTERN:
                if let lantern = sensor as? LanternSensor {
                    LanternSensorView(name: lantern.name, data: lantern.data, distance: Int(lantern.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .CO2:
                if let co2 = sensor as? CO2Sensor {
                    CO2SensorView(data: co2.data, name: co2.name, distance: Int(co2.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            case .WATERLEVEL:
                if let level = sensor as? WaterlevelSensor {
                    WaterlevelSensorView(data: level.data, name: level.name, distance: Int(level.distance), current: current, max: max, opacity: maxOpacity ? 1.0 : opacity)
                }
            default:
                Text("Kein Sensor erkannt")
            }
        }
    }
}
