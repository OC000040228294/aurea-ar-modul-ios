//
//  ARHome.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import SwiftUI
import CityPadFramework

/// The ARHome View is the main View of the AR-Mode.
/// It contains the CameraView and the AROverlayView.
struct ARHome: View {
    @EnvironmentObject private var mainState: MainState
    @EnvironmentObject private var model: ModelData
    /// The ArHomeViewModel is used to get the arItems selected in the compassView from the ModelData.
    @StateObject private var viewModel = ArHomeViewModel()
    @StateObject private var camera = CameraViewModel()
    @StateObject private var arState = ArState(locationModel: LocationModel.shared,currentNearbyThresholdMeters: currentNearbyThresholdMeters)
    @StateObject private var stackViewState = StackViewState(count: 0)
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                /// The CameraView is the background of the AR-Mode.
                CameraView(image: camera.frame)
                    .edgesIgnoringSafeArea(.trailing)
                    .edgesIgnoringSafeArea(.top)
                    .edgesIgnoringSafeArea(.leading)
                /// The AROverlayView from the AureaFramework shows the ARItems at the screen depending on the users and the items location.
                AROverlayView()
                    .onChange(of: viewModel.arItems, perform: { value in
                        arState.setSensors(sensors: viewModel.arItems)
                    })
                    .onAppear() {
                        if (mainState.devModeSimulateLocation) {
                            arState.setDevModeSimulationLocation(location: aureaPosition())
                        }
                        arState.setNearbyLocations(fixedLocation: mainState.fixedLocation, nearbyLocation: mainState.nearbyLocation)
                        arState.useFixedNearbyLocation(mainState.useFixedNearbyLocation)
                        arState.setSensors(sensors: viewModel.arItems)
                    }
                
                if let sensor = arState.sensor {
                    if sensor.withinBounds(geometry.size.width, geometry.size.height) {
                        /// The UseCaseView shows description of the sensor shown on selected arItem when the user taps "more information".
                        UseCaseView()
                            .modifier(BottomBoundModifier())
                            .padding(.bottom, 32)
                            .onTapGesture {
                                if stackViewState.finalCurrentIndex < viewModel.arItems.count {
                                    let sensor = model.sensors.first {
                                        viewModel.arItems[stackViewState.finalCurrentIndex].id == $0.id
                                    }
                                    if let type = sensor?.type {
                                        mainState.setSensorInfo(type: type)
                                    } else {
                                        logger.error("ARHome: Could not find Type of selected Artefact with id: \(viewModel.arItems[stackViewState.finalCurrentIndex].id)")
                                    }
                                } else {
                                    logger.error("ARHome: Selected Artefact index is out of Bounds with viewModels arItems.count")
                                }
                            }
                    } else {
                        /// The TurnAroundView and DirecitonView shows the user where to move to see the selected arItem.
                        if !sensor.isInZViewingDirection {
                            TurnAroundView()
                                .modifier(CenterScreenModifier())
                        } else {
                            let angle = ARCalculations.calcDirection(geometry.size.width, geometry.size.height, sensor.x, sensor.y)
                            DirectionView(angle: angle)
                                .modifier(CenterScreenModifier())
                        }
                    }
                } else {
                    NoSensorSelected()
                }
            }
            .environmentObject(arState)
            .environmentObject(stackViewState)
            .highPriorityGesture(dragGesture)
        }
    }
    
    private var dragGesture: some Gesture {
        /// The dragGesture is used to change the selected arItem in the StackView.
        DragGesture()
            .onChanged { value in
                withAnimation(.interactiveSpring()) {
                    let y = (value.translation.height / 300) - stackViewState.previousIndex
                    stackViewState.currentIndex = -y
                }
            }
            .onEnded { value in
                stackViewState.snapToNearestAbsoluteIndex(value.predictedEndTranslation)
                stackViewState.previousIndex = stackViewState.currentIndex
            }
    }
}

struct ARHome_Previews: PreviewProvider {
    static var previews: some View {
        ARHome()
            .environmentObject(ArState(locationModel: LocationModel.shared,currentNearbyThresholdMeters: 10.0))
            .environmentObject(ModelData.shared)
            .environmentObject(MainState.shared)
            .environmentObject(CameraViewModel())
    }
}
