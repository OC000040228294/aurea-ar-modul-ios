//
//  ArSensor.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import Foundation
import CityPadFramework

public let EXTRA_PADDING = -16.0
public let hoverViewWidth = 280.0
public let hoverViewHeight = 230.0

struct ArSensor: Identifiable {
    var id: String
    var distance: Double
    var sensors: [any Sensor]
    var x: Double
    var y: Double
    var z: Double
    var hitbox: Hitbox
    var focused = false
    
    var isNearby: Bool {
        return distance <= currentNearbyThresholdMeters
    }
    
    var isInZViewingDirection: Bool {
        return z >= 0
    }
    
    var isCluster: Bool {
        return sensors.count > 1
    }
    
    var isSelectable: Bool {
        if isCluster {
            return true
        }
        
        return false
    }
    
    mutating func cluster(other: ArSensor) {
        sensors.append(contentsOf: other.sensors)
        sensors.sort {
            $0.distance < $1.distance
        }
        distance = sensors[0].distance
    }
    
    func canCluster(other: ArSensor) -> Bool {
        if self.isInZViewingDirection && other.isInZViewingDirection && self.hitbox.collidesWith(other: other.hitbox) {
            return true
        } else {
            return false
        }
    }
    
    static let exampleData = ArSensor(id: "0", distance: 500.0, sensors: sensorExampleData, x: 200.0, y: 300.0, z: 0.0, hitbox: Hitbox(left: 50, top: 50, right: 50, bottom: 50))
}

extension ArSensor {
    func withinBounds(_ width: Double, _ height: Double) -> Bool {
        if !self.isInZViewingDirection {
            return false
        }
        
        if x >= -hoverViewWidth && x <= width + hoverViewWidth && y >= -hoverViewHeight && y <= height + hoverViewHeight {
            return true
        } else {
            return false
        }
    }
}
