//
//  ArHomeState.swift
//  Aurea
//
//  Created by Sebastian Lange on 28.09.23.
//

import Foundation
import Combine
import CityPadFramework

/// View model for the AR home view
/// It is used to get the arItems selected in the compassView from the ModelData.
/// It calculates the distance of the sensors to the user.
@MainActor
final class ArHomeViewModel: ObservableObject {
    private var model = ModelData.shared
    private var mainState = MainState.shared
    
    private var locationModel = LocationModel.shared
    private var locationSubscription: AnyCancellable?
    
    @Published var arItems: [ArItem] = []
    
    
    init() {
        locationSubscription = locationModel.$location.sink{[weak self] _ in
            self?.updateSensorDistances()
        }
    }
    
    func updateSensorDistances() {
        var targetedSensors = model.sensors.filter {
            mainState.targetList.contains($0.id)
        }.map {
            let location = mainState.devModeSimulateLocation == true ? aureaPosition() :  locationModel.location
            var sensor = $0
            sensor.distance = location.distance(from: $0.location)
            return sensor
        }
        targetedSensors.sort(by: { a, b in
            a.distance < b.distance
        })
        
        var items: [ArItem] = []
        
        for i in (0..<targetedSensors.count) { 
            items.append(targetedSensors[i].toArItem(current: i+1, max: targetedSensors.count, maxOpacity: targetedSensors.count > 1))
        }
        
        arItems = items
    }
}
