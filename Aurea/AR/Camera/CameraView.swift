//
//  CameraView.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import SwiftUI

struct CameraView: View {
    var image: CGImage?
    private let label = Text("Camera Feed")
    
    var body: some View {
        if let image = image {
            GeometryReader {geometry in
                Image(image, scale: 1.0, orientation: .up, label: label)
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                    .clipped()
            }
        } else {
            Color.black
        }
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        CameraView()
    }
}
