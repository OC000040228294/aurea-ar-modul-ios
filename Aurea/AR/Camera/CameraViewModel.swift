//
//  CameraViewModel.swift
//  CityPad
//
//  Created by lichen on 18.05.22.
//

import Foundation
import CoreImage

class CameraViewModel: ObservableObject {
    @Published var frame: CGImage?
    
    private let frameManager = FrameManager.shared
    
    init() {
        setupSubscriptions()
    }
    
    func setupSubscriptions() {
        frameManager.$current
            .receive(on: RunLoop.main)
            .compactMap {buffer in
                return CGImage.create(from: buffer)
            }
            .assign(to: &$frame)
    }
}
