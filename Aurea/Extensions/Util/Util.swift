//
//  Util.swift
//  CityPad
//
//  Created by lichen on 30.05.22.
//

import Foundation
import SwiftUI
import CoreLocation

let EARTH_RADIUS = 6371009.0
let MAX_DISTANCE_FROM_MAIN_AREA = 500.0
let centerOfMainArea = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 51.30124136846982, longitude: 9.5001839602235), altitude: 150, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now) // set to auebad location for now

func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

func formatDate(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateStyle = .long
    return formatter.string(for: date) ?? ""
}

func getBearingBetweenTwoPoints(point1 : CLLocation, point2 : CLLocation) -> Double {
    let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
    let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)

    let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
    let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)

    let dLon = lon2 - lon1

    let y = sin(dLon) * cos(lat2)
    let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
    let radiansBearing = atan2(y, x)

    return radiansToDegrees(radians: radiansBearing)
}

enum Haptics {
    case Selected
    case Unselected
}

func randomNearbyLocation(lng: Double, lat: Double, radius: Double, alt: Double) -> CLLocation {
    let radiusInDegrees = radius / 111000.0
    let u = Double.random(in: 0.0...1.0)
    let v = Double.random(in: 0.0...1.0)
    let w = radiusInDegrees * sqrt(u)
    let t = 2 * Double.pi * v
    let x = w * cos(t)
    let y = w * sin(t)
    
    let newX = x / cos(lat.toRadians())
    
    let foundLng = newX + lng
    let foundLat = y + lat
    
    return CLLocation(coordinate: CLLocationCoordinate2D(latitude: foundLat, longitude: foundLng), altitude: alt, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now)
}

func manufactureNearbyLocation(location: CLLocation, distance: Double = 10, heading: Double) -> CLLocation {
    let dist = distance / EARTH_RADIUS
    let headingRadians = heading.toRadians()
    let latRadians = location.coordinate.latitude.toRadians()
    let lngRadians = location.coordinate.longitude.toRadians()
    let cosDist = cos(dist)
    let sinDist = sin(dist)
    let sinFromLat = sin(latRadians)
    let cosFromLat = cos(latRadians)
    let sinLat = cosDist * sinFromLat + sinDist * cosFromLat * cos(headingRadians)
    let dLng = atan2(
        sinDist * cosFromLat * sin(headingRadians),
        cosDist - sinFromLat * sinLat
    )
    
    let lat = radiansToDegrees(radians: asin(sinLat))
    let lng = radiansToDegrees(radians: lngRadians + dLng)
    
    return CLLocation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), altitude: location.altitude, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now)
}
