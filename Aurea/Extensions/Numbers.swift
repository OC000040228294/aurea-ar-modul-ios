//
//  Numbers.swift
//  CityPad
//
//  Created by lichen on 08.08.22.
//

import Foundation

extension Double {
    func toRadians() -> Double {
        return self * 0.017453292519943295
    }
}

extension Int {
    func NsAsUInt() -> UInt64 {
        return UInt64(self * 1_000_000_000)
    }
}
