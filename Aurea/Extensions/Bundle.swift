//
//  Bundle.swift
//  Aurea
//
//  Created by lichen on 30.05.23.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0.0"
    }
    
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? "1.0.0"
    }
}
