//
//  ContentView.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import SwiftUI
import CorePermissionsSwiftUI
import PermissionsSwiftUICamera
import PermissionsSwiftUILocation

/// ContentView is the main entry point of the app.
///
/// It is responsible for displaying the onboarding view or the other views depending on the onboarding state.
struct ContentView: View {
    @AppStorage("onboarding") var isOnboardingViewActive: Bool = true
    
    @StateObject private var tabState = TabStateManager.shared
    @State private var showModal = false
    @State private var selectedItem = 2
    
    var body: some View {
        if isOnboardingViewActive {
            OnboardingView(permissions: $showModal)
                .JMModal(showModal: $showModal, for: [.location, .camera])
                .setPermissionComponent(for: .camera, title: "Kamera")
                .setPermissionComponent(for: .camera, description: "Erlaube die Benutzung der Kamera")
                .setPermissionComponent(for: .location, title: "Standort")
                .setPermissionComponent(for: .location, description: "Erlaube den Zugriff auf den Standort")
        } else {
            TabView(selection: $selectedItem) {
                InfoHome()
                    .tabItem {
                        Label("Über Aurea", systemImage: "info.circle")
                    }
                    .tag(1)
                
                MainHome()
                    .tabItem {
                        Label("Entdecken", systemImage: "location.north.fill")
                    }
                    .tag(2)
                
                SettingsHome()
                    .tabItem {
                        Label("Einstellungen", systemImage: "gearshape.fill")
                    }
                    .tag(3)
                
                ContactHome()
                    .tabItem {
                        Label("Kontakt", systemImage: "envelope.fill")
                    }
                    .tag(4)
            }
            .environmentObject(tabState)
            .tint(Color(.smartBlue))
            .onAppear {
                let tabBarAppearance = UITabBarAppearance()
                tabBarAppearance.configureWithDefaultBackground()
                UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
            }
            .onChange(of: selectedItem) {value in
                if value != 2 {
                    tabState.setOtherState()
                } else {
                    tabState.setMainState()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData.shared)
            .environmentObject(MainState.shared)
            .defaultAppStorage(skipOnboarding())
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
    }
}

func skipOnboarding() -> UserDefaults {
    let defaults = UserDefaults.standard
    defaults.set(false, forKey: "onboarding")
    
    return defaults
}
