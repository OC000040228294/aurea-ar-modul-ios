//
//  CityPadApp.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import SwiftUI

@main
struct CityPadApp: App {    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
