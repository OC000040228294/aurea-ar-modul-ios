//
//  NoLocationCard.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct NoLocationCard: View {
    var body: some View {
        VStack {
            Text("Berechtigungen benötigt")
                .bold()
                .padding(.bottom, 8)
                .font(.title2)
                .foregroundColor(.white)
                
            Text("Um das Augmented Reality Feature nutzen zu können, erteile bitte die Berechtigungen für Standort und Kamera in den Einstellungen.")
                .font(.title3)
                .padding(.horizontal, 32)
                .foregroundColor(.white)
        }
        .padding(.vertical, 32)
        .background(RoundedRectangle(cornerSize: CGSize(width: 5, height: 5)).foregroundColor(Color(.smartBlue)).shadow(radius: 5))
    }
}

struct NoLocationCard_Previews: PreviewProvider {
    static var previews: some View {
        NoLocationCard()
    }
}
