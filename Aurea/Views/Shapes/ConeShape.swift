//
//  ConeShape.swift
//  Aurea
//
//  Created by lichen on 12.05.23.
//

import SwiftUI

struct ConeShape: View {
    let degree: Int
    var radarRadiusInPercent = 0.9
    
    var body: some View {
        Cone(degree: degree, radarRadiusInPercent: radarRadiusInPercent)
    }
}

struct ConeShape_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            
            Cone(degree: 30, radarRadiusInPercent: 0.95)
                .fill(LinearGradient(colors: [Color(.smartBlue), .gray], startPoint: .top, endPoint: .bottom))
                .opacity(0.15)
            
            Cone(degree: 15, radarRadiusInPercent: 0.95)
                .fill(LinearGradient(colors: [Color(.smartBlue), .gray], startPoint: .top, endPoint: .bottom))
                .opacity(0.4)
        }
        .frame(width: 400, height: 400)
        
    }
}

/// Cone shape.
/// - Parameters:
///   - degree: The degree of the cone.
///   - radarRadiusInPercent: The radius of the cone.
struct Cone: Shape {
    let degree: Int
    var radarRadiusInPercent = 0.9
    
    var leftPointDegree: Double {
        return Double(-1 * (degree / 2))
    }
    
    var rightPointDegree: Double {
        return Double(degree / 2)
    }
    
    func calcOffset(_ x: Int, _ y: Int, _ angle: Double, _ radarRadius: Double) -> (Double, Double) {
        let artefactRadius = radarRadius
        let deg = angle
        
        let radians = deg.toRadians()
        
        let newX = Double(x) + sin(radians) * artefactRadius
        let newY = Double(y) - cos(radians) * artefactRadius
        
        return (newX, newY)
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let radius = rect.midY * radarRadiusInPercent
        
        let (leftX, leftY) = calcOffset(Int(rect.midX), Int(rect.midY), leftPointDegree, radius)
        let (rightX, rightY) = calcOffset(Int(rect.midX), Int(rect.midY), rightPointDegree, radius)
        
        path.move(to: CGPoint(x: rect.midX, y: rect.midY))
        path.addLine(to: CGPoint(x: leftX, y: leftY))
        path.addQuadCurve(to: CGPoint(x: rightX, y: rightY), control: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.midY))
        
        return path
    }
}
