//
//  SwimmingPoolSensor.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct SwimmingPoolSensorView: View {
    let data: SwimmingPoolData
    let distance: Int
    let name: String
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.SwimmingPool.rawValue
    var opacity = hoverOpacity
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)
            
                Spacer()
                
                SwimmingPoolDisplayView(data: data)
                    .padding(.bottom, 24)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
            
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct SwimmingPoolSensorView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            ContentView()
            Image(.cameraPreviewTree)
                .resizable()
            SwimmingPoolSensorView(data: SwimmingPoolData(sauna: 15, outdoor: 0, indoor: 250, sauna_capacity: .MEDIUM, outdoor_capacity: .LOW, indoor_capacity: .HIGH), distance: 30, name: "Auebad", current: 19, max: 20, opacity: 0.8)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            SwimmingPoolSensorView(data: SwimmingPoolData(sauna: 15, outdoor: 0, indoor: 350, sauna_capacity: .MEDIUM, outdoor_capacity: .LOW, indoor_capacity: .HIGH), distance: 50, name: "Auebad", opacity: 0.8)
                .previewLayout(.sizeThatFits)
        }
    }
}
