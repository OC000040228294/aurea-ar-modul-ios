//
//  CO2SensorView.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import SwiftUI

struct CO2SensorView: View {
    var data: CO2Data
    var name: String
    var distance: Int
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.CO2.rawValue
    var opacity = hoverOpacity
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)
                
                Spacer()
                
                CO2DisplayView(data: data)
                    .padding(.bottom, 32)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct CO2SensorView_Previews: PreviewProvider {
    static var previews: some View {
        let data = CO2Data(ppm: "100", classification: .normal)
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            CO2SensorView(data: data, name: "CO2 Sensor", distance: 999)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            CO2SensorView(data: data, name: "CO2 Sensor", distance: 999, current: 3, max: 20)
                .previewLayout(.sizeThatFits)
        }
    }
}
