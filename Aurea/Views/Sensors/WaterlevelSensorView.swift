//
//  WaterlevelSensorView.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import SwiftUI

struct WaterlevelSensorView: View {
    var data: WaterlevelData
    var name: String
    var distance: Int
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.Waterlevel.rawValue
    var opacity = hoverOpacity
    
    var formattedLevel: String {
        NumberFormatter.localizedString(from: NSNumber(value: Int(data.level) ?? 0), number: NumberFormatter.Style.decimal)
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)
                
                Spacer()
                
                VStack(spacing: 8) {
                    Text("Wasserstand in mm:")
                        .font(.title2)
                        .foregroundColor(.black)
                    Text("\(formattedLevel)")
                        .font(.title2)
                        .foregroundColor(Color(.smartBlue))
                }
                .padding(.bottom, 32)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct WaterlevelSensorView_Previews: PreviewProvider {
    static var previews: some View {
        let data = WaterlevelData(level: "2000")
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            WaterlevelSensorView(data: data, name: "Wasserpegel", distance: 9999)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            WaterlevelSensorView(data: data, name: "Wasserpegel", distance: 999, current: 3, max: 20)
                .previewLayout(.sizeThatFits)
        }
    }
}
