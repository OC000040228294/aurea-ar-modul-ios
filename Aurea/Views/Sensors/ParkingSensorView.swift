//
//  ParkingSensorView.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import SwiftUI

struct ParkingSensorView: View {
    var data: ParkingData
    var name: String
    var distance: Int
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.Parking.rawValue
    var opacity = hoverOpacity
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)
                
                Spacer()
                
                ParkingDisplayView(data: data)
                    .modifier(CenterBoundModifier())
                    .padding(.bottom, 24)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct ParkingSensorView_Previews: PreviewProvider {
    static var previews: some View {
        let parkingData = ParkingData(free1: false, free2: true)
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            ParkingSensorView(data: parkingData, name: "Smart Parking", distance: 399, current: 19, max: 20)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            ParkingSensorView(data: parkingData, name: "Smart Parking", distance: 399)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            ParkingSensorView(data: parkingData, name: "Smart Parking", distance: 399, current: 19, max: 20)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            ParkingSensorView(data: parkingData, name: "Smart Parking", distance: 399)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Color.white
            ParkingSensorView(data: parkingData, name: "Smart Parking", distance: 399)
                .previewLayout(.sizeThatFits)
        }
    }
}
