//
//  TrafficSensorView.swift
//  CityPad
//
//  Created by lichen on 28.09.22.
//

import SwiftUI

struct TrafficSensorView: View {
    var data: TrafficData
    var name: String
    var distance: Int
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.TrafficSensor.rawValue
    var opacity = hoverOpacity
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)
            
                Spacer()
                Text("Heute")
                    .foregroundColor(.black)
                    .font(.title2)
                    .fontWeight(.medium)
                TrafficDisplayView(trafficData: data)
                    .padding(.bottom, 32)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
            
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct TrafficSensorView_Previews: PreviewProvider {
    static var model: ModelData = ModelData.shared
    
    static var previews: some View {
        let trafficData = TrafficData(hugeSum: 51, largeSum: 2002, mediumSum: 100, smallSum: 20)

        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            TrafficSensorView(data: trafficData, name: "Verkehr", distance: 277, current: 19, max: 20)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            TrafficSensorView(data: trafficData, name: "Verkehr", distance: 277)
                .previewLayout(.sizeThatFits)
                .preferredColorScheme(.dark)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            TrafficSensorView(data: trafficData, name: "Verkehr", distance: 277)
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Color.white
            TrafficSensorView(data: trafficData, name: "Verkehr", distance: 277)
                .previewLayout(.sizeThatFits)
        }
    }
}
