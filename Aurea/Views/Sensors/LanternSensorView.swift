//
//  LanternSensorView.swift
//  CityPad
//
//  Created by lichen on 05.01.23.
//

import SwiftUI

struct LanternSensorView: View {
    var name: String
    let data: LanternData
    var distance: Int
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.Lantern.rawValue
    var opacity = hoverOpacity
    
    var intensity_icon: String {
        if data.brightness == 0 {
            return "light_intensity_0"
        } else if data.brightness <= 10 {
            return "light_intensity_10"
        } else if data.brightness <= 50 {
            return "light_intensity_50"
        } else {
            return "light_intensity_100"
        }
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)

                Spacer()
                
                HStack(alignment: .center, spacing: 8) {
                    Image(intensity_icon)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 60)
                    Text("Helligkeit: \(data.brightness)%")
                        .font(.title3)
                        .foregroundColor(.black)
                }
                .padding(.bottom, 32)
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct LanternSensorView_Previews: PreviewProvider {
    static var model: ModelData = ModelData.shared
    
    static var previews: some View {
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            ScrollView {
                VStack {
                    LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 0), distance: 400, current: 19, max: 20)
                        .padding()
                    .previewLayout(.sizeThatFits)
                    LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 30), distance: 400, current: 19, max: 20)
                        .padding()
                        .previewLayout(.sizeThatFits)
                    LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 60), distance: 400, current: 19, max: 20)
                        .padding()
                        .previewLayout(.sizeThatFits)
                    LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 90), distance: 400, current: 19, max: 20)
                        .padding()
                        .previewLayout(.sizeThatFits)
                }
            }
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 30), distance: 400)
                .padding()
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Color.white
            
            LanternSensorView(name: "Laterne 2", data: LanternData(brightness: 30), distance: 400)
                .padding()
                .previewLayout(.sizeThatFits)
        }
    }
}
