//
//  PlantSensorView.swift
//  CityPad
//
//  Created by lichen on 28.09.22.
//

import SwiftUI

struct PlantSensorView: View {
    var name: String
    var data: PlantData
    var distance: Int
    var onTap: ((SensorInfo) -> Void)? = nil
    var current: Int = 0
    var max: Int = 0
    let type: String = ImageString.PlantSensor.rawValue
    var opacity = hoverOpacity
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                SensorTitleView(name: name, type: type, opacity: opacity)

                HStack {
                    Text("20cm")
                        .font(.title3)
                        .foregroundColor(.black)

                    HumidityDisplayView(moisture: data.moisture1)
                }
                
                HStack {
                    Text("50cm")
                        .font(.title3)
                        .foregroundColor(.black)

                    HumidityDisplayView(moisture: data.moisture2)
                }
                
                HStack {
                    Text("80cm")
                        .font(.title3)
                        .foregroundColor(.black)

                    HumidityDisplayView(moisture: data.moisture3)
                }
                
                Spacer()
            }
            .background(.white.opacity(opacity))
            .modifier(CardModifier())
            
            DistanceView(distance: distance, opacity: opacity)
            
            ClusterNumberView(current: current, max: max)
        }
        .frame(width: hoverViewWidth, height: hoverViewHeight)
    }
}

struct PlantSensorView_Previews: PreviewProvider {
    static var model: ModelData = ModelData.shared
    
    static var previews: some View {
        let data = PlantData(moisture1: .low, moisture2: .medium, moisture3: .high)
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            PlantSensorView(name: "Baumsensor 2", data: data, distance: 400, current: 19, max: 20)
                .padding()
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            PlantSensorView(name: "Baumsensor 2", data: data, distance: 400)
                .padding()
                .previewLayout(.sizeThatFits)
        }
        
        ZStack {
            Color.white
            
            PlantSensorView(name: "Baumsensor 2", data: data, distance: 400)
                .padding()
                .previewLayout(.sizeThatFits)
        }
    }
}
