//
//  TrafficDisplayView.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct TrafficDisplayView: View {
    let trafficData: TrafficData
    
    var body: some View {
        HStack {
            VStack {
                HugeVehicle()
                Text("\(trafficData.hugeSum)")
                    .font(.headline)
                    .foregroundColor(.black)
            }
            VStack {
                LargeVehicle()
                Text("\(trafficData.largeSum)")
                    .font(.headline)
                    .foregroundColor(.black)
            }
            VStack {
                MediumVehicle()
                Text("\(trafficData.mediumSum)")
                    .font(.headline)
                    .foregroundColor(.black)
            }
            
            VStack {
                SmallVehicle()
                Text("\(trafficData.smallSum)")
                    .font(.headline)
                    .foregroundColor(.black)
            }
        }
    }
}

struct TrafficDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        let trafficData = TrafficData(hugeSum: 51, largeSum: 2002, mediumSum: 100, smallSum: 20)
        
        TrafficDisplayView(trafficData: trafficData)
            .padding()
            .previewLayout(.fixed(width: 300, height: 180))
    }
}
