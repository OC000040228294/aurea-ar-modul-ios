//
//  ClusterCountView.swift
//  CityPad
//
//  Created by lichen on 21.10.22.
//

import SwiftUI

struct ClusterCountView: View {
    var count: Int
    
    var body: some View {
        Text("\(count)")
            .padding(8)
            .background(Circle().foregroundColor(Color(.darkBlue)))
            .foregroundColor(.white)
    }
}

struct ClusterCountView_Previews: PreviewProvider {
    static var previews: some View {
        ClusterCountView(count: 5)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
