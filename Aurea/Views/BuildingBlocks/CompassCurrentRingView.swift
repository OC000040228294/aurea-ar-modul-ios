//
//  CompassCurrentRingView.swift
//  CityPad
//
//  Created by lichen on 23.03.23.
//

import SwiftUI

struct CompassCurrentRingView: View {
    let currentRing: RingStatus
    let width: Double
    
    var body: some View {
        ZStack {
            switch currentRing {
            case .nearby:
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.1), lineWidth: 15)
                    .frame(width: width * NEARBY_RING_RADIUS)
                
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.3), lineWidth: 4)
                    .frame(width: width * NEARBY_RING_RADIUS)
            case .inner:
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.1), lineWidth: 15)
                    .frame(width: width * compassRingRadius1)
                
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.3), lineWidth: 4)
                    .frame(width: width * compassRingRadius1)
            case .middle:
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.1), lineWidth: 20)
                    .frame(width: width * compassRingRadius2)
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.3), lineWidth: 5)
                    .frame(width: width * compassRingRadius2)
            case .outer:
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.1), lineWidth: 30)
                    .frame(width: width * compassRingRadius3)
                Circle()
                    .stroke(Color(.smartBlue).opacity(0.3), lineWidth: 8)
                    .frame(width: width * compassRingRadius3)
            }
        }
    }
}

struct CompassCurrentRingView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geo in
            ZStack {
                CompassView(rotation: 0)
                CompassCurrentRingView(currentRing: .nearby, width: geo.size.width)
            }
        }
        
        GeometryReader {geo in
            ZStack {
                CompassView(rotation: 0)
                CompassCurrentRingView(currentRing: .inner, width: geo.size.width)
            }
        }
        
        GeometryReader {geo in
            ZStack {
                CompassView(rotation: 0)
                CompassCurrentRingView(currentRing: .middle, width: geo.size.width)
            }
        }
        
        GeometryReader {geo in
            ZStack {
                CompassView(rotation: 0)
                CompassCurrentRingView(currentRing: .outer, width: geo.size.width)
            }
        }
    }
}
