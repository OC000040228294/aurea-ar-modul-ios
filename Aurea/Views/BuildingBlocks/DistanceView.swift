//
//  DistanceView.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct DistanceView: View {
    let distance: Int
    let opacity: Double
    
    var distanceText: String {
        if Double(distance) <= NEARBY_THRESHHOLD_LOW {
            "0-\(Int(currentNearbyThresholdMeters))m"
        }
        else if distance < 1000 {
            "\(distance)m"
        } else {
            "\(String.localizedStringWithFormat("%.2f", Float(distance)/1000))km"
        }
    }
    
    var body: some View {
        HStack() {
            Distance()
                .padding(.leading)
            Spacer()
            Text(distanceText)
                .foregroundColor(.white)
                .padding(.trailing)
            Spacer()
        }
        .frame(width: 140, height: 30)
        .background(
            RoundedRectangle(cornerRadius: 25)
                .fill(Color(.darkBlue)).opacity(opacity))
        .padding(EdgeInsets(top: 8, leading: 0, bottom: -16, trailing: 0))
        .modifier(BottomBoundModifier())
    }
}

struct DistanceView_Previews: PreviewProvider {
    static var previews: some View {
        DistanceView(distance: 2, opacity: 0.5)
            .padding()
            .previewLayout(.sizeThatFits)
        
        DistanceView(distance: 999, opacity: 0.5)
            .padding()
            .previewLayout(.sizeThatFits)
        
        DistanceView(distance: 87656, opacity: 0.5)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
