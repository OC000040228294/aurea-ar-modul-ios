//
//  SwimmingPoolDisplayView.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct SwimmingPoolDisplayView: View {
    let data: SwimmingPoolData
    
    var body: some View {
        HStack(spacing: 16) {
            VStack {
                Indoor()
                HStack {
                    Image(getOccupancyIcon(data.indoor_capacity))
                        .resizable()
                        .scaledToFit()
                        .frame(height: 25)
                    Text("\(data.indoor)")
                        .foregroundColor(.black)
                        .font(.headline)
                }
            }
            VStack {
                Outdoor()
                HStack {
                    Image(getOccupancyIcon(data.outdoor_capacity))
                        .resizable()
                        .scaledToFit()
                        .frame(height: 25)
                    Text("\(data.outdoor)")
                        .foregroundColor(.black)
                        .font(.headline)
                }
            }
            VStack {
                Sauna()
                HStack {
                    Image(getOccupancyIcon(data.sauna_capacity))
                        .resizable()
                        .scaledToFit()
                        .frame(height: 25)
                    Text("\(data.sauna)")
                        .foregroundColor(.black)
                        .font(.headline)
                }
            }
        }
    }
}

struct SwimmingPoolDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 16) {
            SwimmingPoolDisplayView(data: SwimmingPoolData(sauna: 2, outdoor: 9, indoor: 8, sauna_capacity: .LOW, outdoor_capacity: .LOW, indoor_capacity: .LOW))
                .previewLayout(.sizeThatFits)
                .padding()
            
            SwimmingPoolDisplayView(data: SwimmingPoolData(sauna: 60, outdoor: 1500, indoor: 500, sauna_capacity: .MEDIUM, outdoor_capacity: .MEDIUM, indoor_capacity: .MEDIUM))
                .previewLayout(.sizeThatFits)
                .padding()
            
            SwimmingPoolDisplayView(data: SwimmingPoolData(sauna: 80, outdoor: 2000, indoor: 700, sauna_capacity: .HIGH, outdoor_capacity: .HIGH, indoor_capacity: .HIGH))
                .previewLayout(.sizeThatFits)
                .padding()
        }
    }
}
