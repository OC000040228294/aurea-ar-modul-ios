//
//  HumidityDisplayView.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import SwiftUI

struct HumidityDisplayView: View {
    var moisture: PlantMoisture
    
    var body: some View {
        switch moisture {
        case .low:
            HStack {
                DropFull()
                DropEmpty()
                DropEmpty()
            }
        case .medium:
            HStack {
                DropFull()
                DropFull()
                DropEmpty()
            }
        case .high:
            HStack {
                DropFull()
                DropFull()
                DropFull()
            }
        }
    }
}

struct HumidityDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        HumidityDisplayView(moisture: .low)
            .padding()
            .previewLayout(.sizeThatFits)
        
        HumidityDisplayView(moisture: .medium)
            .padding()
            .previewLayout(.sizeThatFits)
        
        HumidityDisplayView(moisture: .high)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
