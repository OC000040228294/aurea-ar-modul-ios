//
//  ParkingDisplayView.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import SwiftUI

struct BottomOpenRectangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()

        path.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))

        return path
    }
}

struct ParkingDisplayView: View {
    let data: ParkingData
    
    var body: some View {
        VStack {
            HStack {
                VStack {
                    ZStack {
                        BottomOpenRectangle()
                            .stroke(.black, lineWidth: 5)
                        
                        VStack {
                            
                            ParkingStatus(free: data.free1)
                            
                            Text(data.free1 ? "Frei" : "Belegt")
                                .font(.headline)
                                .foregroundColor(.black)
                        }
                    }
                    .frame(width: 90, height: 120)
                }
                VStack {
                    ZStack {
                        BottomOpenRectangle()
                            .stroke(.black, lineWidth: 5)
                        
                        VStack {
                            
                            ParkingStatus(free: data.free2)
                            
                            Text(data.free2 ? "Frei" : "Belegt")
                                .font(.headline)
                                .foregroundColor(.black)
                        }
                    }
                    .frame(width: 90, height: 120)
                }
            }
        }
    }
}

struct ParkingDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        ParkingDisplayView(data: ParkingData(free1: false, free2: true))
            .padding()
            .previewLayout(.sizeThatFits)
        
        ParkingDisplayView(data: ParkingData(free1: false, free2: true))
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
