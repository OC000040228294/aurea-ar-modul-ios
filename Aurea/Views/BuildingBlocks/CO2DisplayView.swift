//
//  CO2DisplayView.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import SwiftUI

struct CO2DisplayView: View {
    var data: CO2Data
    
    var classification: String {
        switch data.classification {
        case .normal:
            return "unbedenklich"
        case .noticeable:
            return "lüften"
        default:
            return "dringend lüften"
        }
    }
    
    var body: some View {
        VStack(alignment: .center) {
            HStack {
                Image(ImageString.CO2.rawValue)
                    .resizable()
                    .scaledToFit()
                .frame(width: 100)
                Text("\(data.ppm) ppm")
                    .font(.title3)
                    .foregroundColor(.black)
            }
            Text("\(classification)")
                .font(.title)
                .foregroundColor(Color(.smartBlue))
        }
    }
}

struct CO2DisplayView_Previews: PreviewProvider {
    static var previews: some View {
        CO2DisplayView(data: CO2Data(ppm: "500", classification: .normal))
        CO2DisplayView(data: CO2Data(ppm: "500", classification: .noticeable))
        CO2DisplayView(data: CO2Data(ppm: "500", classification: .unacceptable))
    }
}
