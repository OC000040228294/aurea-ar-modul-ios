//
//  SensorTitleView.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct SensorTitleView: View {
    let name: String
    let type: String
    var opacity = hoverOpacity
    
    var body: some View {
            ZStack {
                Text(name)
                    .lineLimit(1)
                    .font(.title2)
                    .foregroundColor(.white)
                    .padding(.horizontal, 30)
                    .frame(maxWidth: .infinity)
                    .overlay(alignment: .leading) {
                        Image(type)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30)
                    }
                    .padding()
            }
            .background(Color(.darkBlue).opacity(opacity))
            .padding(.bottom, 16)
    }
}

struct SensorTitleView_Previews: PreviewProvider {
    static var model: ModelData = ModelData.shared
    
    static var previews: some View {
        SensorTitleView(name: "Verkehrsaufkommen", type: ImageString.TrafficSensor.rawValue)
            .padding()
            .frame(width: 300, height: 80)
    }
}
