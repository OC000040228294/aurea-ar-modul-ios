//
//  SensorInfoView.swift
//  CityPad
//
//  Created by lichen on 16.01.23.
//

import SwiftUI

struct SensorInfoView: View {
    let type: SensorType
    
    var body: some View {
        HTMLView(content: useCaseInfo(type: type))
    }
}

struct SensorInfoView_Previews: PreviewProvider {
    static var previews: some View {
        SensorInfoView(
            type: SensorType.PLANT
        )
    }
}
