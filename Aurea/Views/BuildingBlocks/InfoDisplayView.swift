//
//  InfoDisplayView.swift
//  CityPad
//
//  Created by lichen on 24.10.22.
//

import SwiftUI

struct InfoDisplayView: View {
    let data: InfoData
    
    var body: some View {
        ScrollView {
            Text(data.info)
                .padding(.horizontal)
        }
    }
}

struct InfoDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        InfoDisplayView(data: InfoData(info: "Info Text"))
            .previewLayout(.sizeThatFits)
    }
}
