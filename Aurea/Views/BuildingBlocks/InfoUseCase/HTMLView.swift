//
//  HTMLView.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI
import WebKit

struct HTMLView: View {
    let content: String
    
    var body: some View {
        VStack {
            HTMLStringView(htmlContent: content)
            
            Spacer()
        }
    }
}

struct HTMLStringView: UIViewRepresentable {
    let htmlContent: String

    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator()
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        let fontName =  "PFHandbookPro-Regular"
        let fontSize = 30
        let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
        
        uiView.navigationDelegate = context.coordinator
        uiView.loadHTMLString(fontSetting + htmlContent, baseURL: nil)
    }
}

struct HTMLView_Previews: PreviewProvider {
    static var previews: some View {
        HTMLView(content: "<h1>This is HTML String</h1>\n<h2>This is HTML String</h2>")
    }
}

class Coordinator : NSObject, WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }
        
        url.isReachable { success in //To test if the URL is reachable
            if success {
                decisionHandler(.cancel)
                DispatchQueue.main.async { //Needs to run on the main thread to prevent UI issues
                    UIApplication.shared.open(url)
                }
            } else {
                decisionHandler(.allow)
            }
        }
    }
}

extension URL {
    func isReachable(completion: @escaping (Bool) -> ()) {
        var request = URLRequest(url: self)
        request.httpMethod = "HEAD"
        URLSession.shared.dataTask(with: request) { _, response, _ in
            completion((response as? HTTPURLResponse)?.statusCode == 200)
        }.resume()
    }
}
