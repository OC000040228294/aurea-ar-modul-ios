//
//  NoSensorSelected.swift
//  CityPad
//
//  Created by lichen on 06.12.22.
//

import SwiftUI

struct NoSensorSelected: View {
    var body: some View {
        ZStack {
            VStack(spacing: 16) {
                Text("Kein Sensor eingerastet")
                    .font(.headline)
                    .foregroundColor(.white)
                
                Text("Senke dein Smartphone und zeige im Entdeckermodus/Kompass auf einen Sensor.")
                    .foregroundColor(.white)
            }
            .padding()
        }
        .background(.smartBlue)
        .modifier(CardModifier())
        .padding()
    }
}

struct NoSensorSelected_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            NoSensorSelected()
        }
        
        ZStack {
            Image(.cameraPreviewTree)
                .resizable()
            
            NoSensorSelected()
                .preferredColorScheme(.dark)
        }
    }
}
