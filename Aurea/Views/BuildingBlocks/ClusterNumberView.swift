//
//  ClusterNumberView.swift
//  CityPad
//
//  Created by lichen on 05.02.23.
//

import SwiftUI

struct ClusterNumberView: View {
    let current: Int
    let max: Int
    
    var body: some View {
        if current != 0 && max > 1 {
            HStack {
                Spacer()
                Text("\(current)/\(max)")
                    .foregroundColor(.white)
                    .padding(8)
                    .background(Color(.smartBlue))
                    .cornerRadius(7)
            }
            .offset(x: 24, y: -16)
        }
    }
}

struct ClusterNumberView_Previews: PreviewProvider {
    static var previews: some View {
        ClusterNumberView(current: 19, max: 20)
    }
}
