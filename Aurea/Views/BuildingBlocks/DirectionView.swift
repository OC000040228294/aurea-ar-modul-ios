//
//  DirectionView.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct DirectionView: View {
    let angle: Double
    
    var body: some View {
        Image(systemName: "arrow.up")
            .foregroundColor(.white)
            .font(.system(size: 150))
            .padding()
            .background(Circle().foregroundColor(Color(.smartBlue)))
            .rotationEffect(Angle(degrees: angle))
    }
}

struct DirectionView_Previews: PreviewProvider {
    static var previews: some View {
        DirectionView(angle: 50)
    }
}
