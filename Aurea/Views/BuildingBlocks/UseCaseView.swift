//
//  UseCaseView.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct UseCaseView: View {
    var body: some View {
        HStack() {
            Info()
                .padding(.leading, 8)
            Text("Weitere Informationen")
                .foregroundColor(.white)
                .padding(.trailing)
                .padding(.vertical, 8)
        }
        .background(
            RoundedRectangle(cornerRadius: 25)
                .fill(Color(.darkBlue)))
    }
}

struct UseCaseView_Previews: PreviewProvider {
    static var previews: some View {
        UseCaseView()
    }
}
