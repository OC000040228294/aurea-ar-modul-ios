//
//  SensorsDiscoveredTopCircle.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI

struct SensorsDiscoveredTopCircle: View {
    let type: String
    let isCluster: Bool
    let distance: Int
    var distanceIsRange: Bool = false
    
    var distanceText: String {
        if distanceIsRange {
            return "0-\(distance)m"
        } else {
            return distance < 1000 ? "\(distance)m" : "\(String.localizedStringWithFormat("%.2f", Float(distance)/1000))km"
        }
    }
    
    var body: some View {
        HStack {
            CompassSensorView(type: type)
                .padding(.leading)
            Spacer()
            Text("Sensor")
                .lineLimit(1)
                .foregroundColor(.white)
                .font(.subheadline.bold())
            Spacer()
            Text(distanceText)
                .foregroundColor(.white)
                .padding(.trailing)
                
        }
        .padding(.vertical, 8)
        .background(
            RoundedRectangle(cornerRadius: 5)
                .fill(Color(.darkBlue)))
        .padding(EdgeInsets(top: -24, leading: 16, bottom: 0, trailing: 16))
    }
}

struct SensorsDiscoveredTopCircle_Previews: PreviewProvider {
    static var previews: some View {
        SensorsDiscoveredTopCircle(type: ImageString.PlantSensor.rawValue, isCluster: true, distance: 5000)
        SensorsDiscoveredTopCircle(type: ImageString.PlantSensor.rawValue, isCluster: false, distance: 50000)
        SensorsDiscoveredTopCircle(type: ImageString.PlantSensor.rawValue, isCluster: true, distance: 5000, distanceIsRange: true)
        SensorsDiscoveredTopCircle(type: ImageString.PlantSensor.rawValue, isCluster: false, distance: 50000, distanceIsRange: true)
    }
}
