//
//  AnimationPhoneToARMode.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI

struct AnimationPhoneToARMode: View {
    var body: some View {
        LottieView(filename: "phone", repeating: 4)
            .frame(width: 100, height: 100)
    }
}

struct AnimationPhoneToARMode_Previews: PreviewProvider {
    static var previews: some View {
        AnimationPhoneToARMode()
    }
}
