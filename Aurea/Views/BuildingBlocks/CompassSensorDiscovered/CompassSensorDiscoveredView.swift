//
//  CompassSensorDiscoveredView.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI

/// Displays information of a discovered compass sensor and a hint to tilt the phone to AR mode
struct CompassSensorDiscoveredView: View {
    let name: String
    let isCluster: Bool
    let type: String
    let distance: Int
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                Text("\(name)\(isCluster ? "\nund Weitere" : "")")
                    .font(.title3.bold())
                    .multilineTextAlignment(.center)
                    .foregroundColor(.white)
                    .padding(.top, 8)
                    .padding(.bottom, 0)
                
                AnimationPhoneToARMode()
                
                Text("Richte dein Smartphone auf")
                    .multilineTextAlignment(.center)
                    .foregroundColor(.white)
                    .padding(.top, 0)
                    .padding(.bottom, 8)
            }
            .padding(.top, 24)
            
            VStack {
                SensorsDiscoveredTopCircle(type: type, isCluster: isCluster, distance: distance)
            }
        }
        .frame(maxWidth: .infinity)
        .background(RoundedRectangle(cornerRadius: 5).fill(Color(.smartBlue)))
    }
}

struct CompassSensorDiscoveredView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geo in
            VStack {
                Spacer()
                CompassSensorDiscoveredView(name: "Baum 1", isCluster: true, type: ImageString.PlantSensor.rawValue, distance: 5000)
            }
            .frame(height: geo.size.height * 1)
        }
        
    }
}
