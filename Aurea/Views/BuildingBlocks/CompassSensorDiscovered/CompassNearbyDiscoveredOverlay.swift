//
//  CompassNearbyDiscoveredOverlay.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct CompassNearbyDiscoveredOverlay: View {
    let name: String
    let type: String
    let distance: Int
    
    let view = LottieView(filename: "phone", repeating: 2.0)
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                Text("\(name) in der Nähe")
                    .font(.title3.bold())
                    .multilineTextAlignment(.center)
                    .foregroundColor(.white)
                    .padding()
                
                LottieView(filename: "phone", repeating: 2.0)
                    .frame(width: 60, height: 60)
                
                Text("Richte dein Smartphone auf")
                    .multilineTextAlignment(.center)
                    .foregroundColor(.white)
                    .padding()
                    .padding(.bottom, 8)
            }
            .padding(.top, 24)
            
            VStack {
                SensorsDiscoveredTopCircle(type: type, isCluster: false, distance: distance, distanceIsRange: true)
            }
        }
        .frame(maxWidth: .infinity)
        .background(RoundedRectangle(cornerRadius: 5).fill(Color(.smartBlue)))
    }
}

struct CompassNearbyDiscoveredOverlay_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geo in
            VStack {
                Spacer()
                CompassNearbyDiscoveredOverlay(name: "Smart parking 1", type: ImageString.Parking.rawValue, distance: 5)
            }
            .frame(height: geo.size.height * 1)
        }
    }
}
