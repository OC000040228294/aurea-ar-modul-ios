//
//  CompassDiscoveredOverlay.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI

/// Displays an overlay for a discovered compass sensor.
struct CompassDiscoveredOverlay: View {
    let name: String
    let isCluster: Bool
    let type: String
    let distance: Int
    
    var body: some View {
        VStack {
            Spacer()
            CompassSensorDiscoveredView(name: name, isCluster: isCluster, type: type, distance: distance)
        }
    }
}

struct CompassDiscoveredOverlay_Previews: PreviewProvider {
    static var previews: some View {
        CompassDiscoveredOverlay(name: "Baum 1", isCluster: true, type: ImageString.PlantSensor.rawValue, distance: 50)
    }
}
