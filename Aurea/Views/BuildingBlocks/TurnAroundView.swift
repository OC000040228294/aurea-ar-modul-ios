//
//  TurnAroundView.swift
//  CityPad
//
//  Created by lichen on 08.03.23.
//

import SwiftUI

struct TurnAroundView: View {
    var body: some View {
        Image(systemName: "arrow.uturn.down")
            .foregroundColor(.white)
            .font(.system(size: 150))
            .padding()
            .background(Circle().foregroundColor(Color(.smartBlue)))
    }
}

struct TurnAroundView_Previews: PreviewProvider {
    static var previews: some View {
        TurnAroundView()
    }
}
