//
//  ClusterNumber.swift
//  CityPad
//
//  Created by lichen on 05.12.22.
//

import SwiftUI

struct ClusterNumber: View {
    let count: Int
    var body: some View {
        ZStack {
            Circle()
                .frame(width: 20)
                .foregroundColor(Color(.darkBlue))
            Text("\(count)")
                .font(.system(size: 12))
                .fontWeight(.bold)
                .foregroundColor(.white)//Color("Background"))
        }
        
    }
}

struct ClusterNumber_Previews: PreviewProvider {
    static var previews: some View {
        ClusterNumber(count: 13)
        ClusterNumber(count: 3)
    }
}
