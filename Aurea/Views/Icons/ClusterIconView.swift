//
//  ClusterIconView.swift
//  CityPad
//
//  Created by lichen on 21.10.22.
//

import SwiftUI

struct ClusterIconView: View {
    let count: Int
    
    var isFocused: Bool = true
    
    var body: some View {
        ZStack {
            if !isFocused {
                MarkerGrey()
            } else {
                Marker()                
            }
           
            ClusterNumber(count: count)
                .offset(x: 12, y: -12)
        }
        
    }
}

struct ClusterIconView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Image(.compassv2)
                .resizable()
                .scaledToFit()
            ClusterIconView(count: 13)
                .previewLayout(.sizeThatFits)
                .padding()
                .offset(x: 50, y: -50)
            
            ClusterIconView(count: 3)
                .previewLayout(.sizeThatFits)
                .padding()
                .offset(x: -50, y: -50)
        }
        .modifier(TopBoundModifier())
        
        ClusterIconView(count: 13)
            .previewLayout(.sizeThatFits)
            .padding()
        
        ClusterIconView(count: 3)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
