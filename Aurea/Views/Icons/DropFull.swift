//
//  DropFull.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import SwiftUI

struct DropFull: View {
    var body: some View {
        Image("drop_full")
            .resizable()
            .scaledToFit()
            .frame(width: 30)
    }
}

struct DropFull_Previews: PreviewProvider {
    static var previews: some View {
        DropFull()
    }
}
