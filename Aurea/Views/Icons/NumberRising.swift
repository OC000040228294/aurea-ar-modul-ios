//
//  NumberRising.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct NumberRising: View {
    var body: some View {
        Image(systemName: "arrow.up.right")
            .font(.headline)
            .foregroundColor(.green)
    }
}

struct NumberRising_Previews: PreviewProvider {
    static var previews: some View {
        NumberRising()
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
