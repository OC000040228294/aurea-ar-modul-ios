//
//  TreeIcon.swift
//  CityPad
//
//  Created by lichen on 08.12.22.
//

import SwiftUI

struct TreeIcon: View {
    var body: some View {
        Image(.tree)
            .resizable()
            .scaledToFit()
            .frame(width: 30)
    }
}

struct TreeIcon_Previews: PreviewProvider {
    static var previews: some View {
        TreeIcon()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
