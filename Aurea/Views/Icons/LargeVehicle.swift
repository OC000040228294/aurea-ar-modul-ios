//
//  LargeVehicle.swift
//  CityPad
//
//  Created by lichen on 08.12.22.
//

import SwiftUI

struct LargeVehicle: View {
    var body: some View {
        Image("vehicle_large")
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct LargeVehicle_Previews: PreviewProvider {
    static var previews: some View {
        LargeVehicle()
    }
}
