//
//  Info.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct Info: View {
    var body: some View {
        Image(systemName: "info.circle.fill")
            .foregroundColor(.white)
            .font(.title2)
    }
}

struct Info_Previews: PreviewProvider {
    static var previews: some View {
        Info()
    }
}
