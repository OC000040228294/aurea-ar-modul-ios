//
//  Outdoor.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct Outdoor: View {
    var body: some View {
        Image(.poolOutdoor)
            .resizable()
            .scaledToFit()
            .frame(width: 40)
    }
}

struct Outdoor_Previews: PreviewProvider {
    static var previews: some View {
        Outdoor()
    }
}
