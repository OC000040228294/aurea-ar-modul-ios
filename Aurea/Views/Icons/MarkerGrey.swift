//
//  MarkerGrey.swift
//  Aurea
//
//  Created by lichen on 22.05.23.
//

import SwiftUI

struct MarkerGrey: View {
    var body: some View {
        Image(.clusterGrey)
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct MarkerGrey_Previews: PreviewProvider {
    static var previews: some View {
        MarkerGrey()
    }
}
