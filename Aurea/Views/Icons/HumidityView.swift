//
//  HumidityView.swift
//  CityPad
//
//  Created by lichen on 05.10.22.
//

import SwiftUI

struct HumidityView: View {
    var body: some View {
        ZStack {
            Image(systemName: "drop.fill")
                .foregroundColor(.black)
                .font(.largeTitle)
            Image(systemName: "thermometer")
                .font(.title3)
                .foregroundColor(.white)
                //.offset(CGSize(width: 15, height: 10))
        }
    }
}

struct HumidityView_Previews: PreviewProvider {
    static var previews: some View {
        HumidityView()
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
