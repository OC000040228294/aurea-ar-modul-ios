//
//  MediumVehicle.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct MediumVehicle: View {
    var body: some View {
        Image("vehicle_medium")
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct MediumVehicle_Previews: PreviewProvider {
    static var previews: some View {
        MediumVehicle()
    }
}
