//
//  HoverIconView.swift
//  CityPad
//
//  Created by lichen on 06.02.23.
//

import SwiftUI

struct HoverIconView: View {
    let type: String
    
    var body: some View {
        Image(type)
            .resizable()
            .scaledToFit()
            .frame(width: 30)
            .padding()
            .modifier(LeftBoundModifier())
    }
}

struct HoverIconView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            HoverIconView(type: ImageString.Lantern.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 0, y: 0)
            
            HoverIconView(type: ImageString.PlantSensor.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: -50, y: -50)
            
            HoverIconView(type: ImageString.Parking.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 50, y: -50)
            
            HoverIconView(type: ImageString.TrafficSensor.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: -50, y: 50)
            
            HoverIconView(type: ImageString.SwimmingPool.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 50, y: 50)
        }
        .offset(x: 150)
    }
}
