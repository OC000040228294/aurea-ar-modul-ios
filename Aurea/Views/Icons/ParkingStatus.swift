//
//  ParkingStatus.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import SwiftUI

struct ParkingStatus: View {
    let free: Bool
    
    var body: some View {
        Image(.parking)
            .resizable()
            .renderingMode(.template)
            .foregroundColor(free ? .green : .red)
            .frame(width: 30, height: 30)
    }
}

struct ParkingStatus_Previews: PreviewProvider {
    static var previews: some View {
        ParkingStatus(free: false)
            .padding()
            .previewLayout(.sizeThatFits)
        
        ParkingStatus(free: true)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
