//
//  DropEmpty.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import SwiftUI

struct DropEmpty: View {
    var body: some View {
        Image("drop_empty")
            .resizable()
            .scaledToFit()
            .frame(width: 30)
    }
}

struct DropEmpty_Previews: PreviewProvider {
    static var previews: some View {
        DropEmpty()
    }
}
