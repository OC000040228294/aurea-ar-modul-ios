//
//  Cube.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import SwiftUI

struct Cube: View {
    var body: some View {
        Image(systemName: "cube.fill")
            .font(.title)
            .foregroundColor(.red)
    }
}

struct Cube_Previews: PreviewProvider {
    static var previews: some View {
        Cube()
    }
}
