//
//  Distance.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct Distance: View {
    var body: some View {
        Image(systemName: "location.fill")
            .font(.headline)
            .foregroundColor(.white)
    }
}

struct Distance_Previews: PreviewProvider {
    static var previews: some View {
        Distance()
            .padding()
            .previewLayout(.sizeThatFits)
            .background(.blue)
    }
}
