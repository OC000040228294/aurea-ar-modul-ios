//
//  SmallVehicle.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct SmallVehicle: View {
    var body: some View {
        Image("vehicle_small")
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct SmallVehicle_Previews: PreviewProvider {
    static var previews: some View {
        SmallVehicle()
    }
}
