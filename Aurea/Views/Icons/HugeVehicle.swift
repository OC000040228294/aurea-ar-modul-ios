//
//  HugeVehicle.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct HugeVehicle: View {
    var body: some View {
        Image("vehicle_huge")
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct HugeVehicle_Previews: PreviewProvider {
    static var previews: some View {
        HugeVehicle()
    }
}
