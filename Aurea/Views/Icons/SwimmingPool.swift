//
//  SwimmingPool.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct SwimmingPool: View {
    var body: some View {
        Image(systemName: "cloud.heavyrain")
            .font(.largeTitle)
    }
}

struct SwimmingPool_Previews: PreviewProvider {
    static var previews: some View {
        SwimmingPool()
    }
}
