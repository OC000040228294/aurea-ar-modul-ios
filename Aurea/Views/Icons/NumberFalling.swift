//
//  NumberFalling.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import SwiftUI

struct NumberFalling: View {
    var body: some View {
        Image(systemName: "arrow.down.right")
            .font(.headline)
            .foregroundColor(.red)
    }
}

struct NumberFalling_Previews: PreviewProvider {
    static var previews: some View {
        NumberFalling()
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
