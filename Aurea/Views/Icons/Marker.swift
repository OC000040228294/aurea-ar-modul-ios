//
//  Marker.swift
//  CityPad
//
//  Created by lichen on 18.11.22.
//

import SwiftUI

struct Marker: View {
    var body: some View {
        Image(.cluster)
            .resizable()
            .scaledToFit()
            .frame(height: 35)
    }
}

struct Marker_Previews: PreviewProvider {
    static var previews: some View {
        Marker()
    }
}
