//
//  LanternIcon.swift
//  CityPad
//
//  Created by lichen on 05.01.23.
//

import SwiftUI

struct LanternIcon: View {
    var body: some View {
        Image(.lantern)
            .resizable()
            .scaledToFit()
            .frame(width: 30)
            .foregroundColor(.blue)
    }
}

struct LanternIcon_Previews: PreviewProvider {
    static var previews: some View {
        LanternIcon()
    }
}
