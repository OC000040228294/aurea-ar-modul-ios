//
//  ParkingCircle.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import SwiftUI

struct ParkingCircle: View {
    var body: some View {
        Image(.parking)
            .resizable()
            .scaledToFit()
            .frame(width: 40)
    }
}

struct ParkingCircle_Previews: PreviewProvider {
    static var previews: some View {
        ParkingCircle()
    }
}
