//
//  DropHalf.swift
//  CityPad
//
//  Created by lichen on 24.11.22.
//

import SwiftUI

struct DropHalf: View {
    var body: some View {
        Image(.dropHalf)
            .resizable()
            .scaledToFit()
            .frame(width: 30)
    }
}

struct DropHalf_Previews: PreviewProvider {
    static var previews: some View {
        DropHalf()
    }
}
