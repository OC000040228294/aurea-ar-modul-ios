//
//  Nearby.swift
//  CityPad
//
//  Created by lichen on 07.11.22.
//

import SwiftUI

struct Nearby: View {
    var body: some View {
        Image(systemName: "mappin")
            .font(.title)
            .foregroundColor(.white)
    }
}

struct Nearby_Previews: PreviewProvider {
    static var previews: some View {
        Nearby()
            .padding()
            .background(.blue)
    }
}
