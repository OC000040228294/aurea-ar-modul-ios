//
//  Thermometer.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import SwiftUI

struct Thermometer: View {
    var body: some View {
        Image(systemName: "thermometer")
            .font(.largeTitle)
    }
}

struct Thermometer_Previews: PreviewProvider {
    static var previews: some View {
        Thermometer()
    }
}
