//
//  Indoor.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct Indoor: View {    
    var body: some View {
        Image(.poolIndoor)
            .resizable()
            .scaledToFit()
            .frame(width: 40)
    }
}

struct Indoor_Previews: PreviewProvider {
    static var previews: some View {
        Indoor()
    }
}
