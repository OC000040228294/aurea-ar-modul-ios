//
//  Sauna.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI

struct Sauna: View {
    var body: some View {
        Image(.poolSauna)
            .resizable()
            .scaledToFit()
            .frame(width: 40)
    }
}

struct Sauna_Previews: PreviewProvider {
    static var previews: some View {
        Sauna()
    }
}
