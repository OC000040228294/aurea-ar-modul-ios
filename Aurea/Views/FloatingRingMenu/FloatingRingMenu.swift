//
//  FloatingRingMenu.swift
//  CityPad
//
//  Created by lichen on 08.05.23.
//

import SwiftUI

/// Floating ring menu for choosing the active compass ring.
/// 
/// The ring menu is a floating button group. It allows the user to choose the active ring.
///
/// - Parameters:
///   - ring: The currently active ring.
///

struct FloatingRingMenu: View {
    @EnvironmentObject private var state: CompassState
    
    @Binding var ring: RingStatus
    
    var width: CGFloat = 56
    var height: CGFloat = 34
    
    var nearbyEnabled = true
    var innerEnabled = true
    var middleEnabled = true
    var outerEnabled = true
    
    var nearbyClicked: () -> Void
    var innerClicked: () -> Void
    var middleClicked: () -> Void
    var outerClicked: () -> Void
    
    var body: some View {
        VStack {
            RoundedRectangle(cornerRadius: 5)
                .frame(width: width, height: height)
                .foregroundColor(ring == .outer ? Color(.smartBlue) : Color(.background).opacity(0))
                .overlay {
                    Text("∞")
                        .foregroundColor(ring == .outer ? .white : Color(.smartBlue))
                }
                .animation(.easeInOut, value: ring)
                .onTapGesture {
                    if outerEnabled {
                        outerClicked()
                    } else {
                        state.ringLimitReachedVibration()
                    }
                }
            
            RoundedRectangle(cornerRadius: 5)
                .frame(width: width, height: height)
                .foregroundColor(ring == .middle ? Color(.smartBlue) : Color(.background).opacity(0))
                .overlay {
                    Text("200m")
                        .foregroundColor(ring == .middle ? .white : Color(.smartBlue))
                }
                .animation(.easeInOut, value: ring)
                .onTapGesture {
                    if middleEnabled {
                        middleClicked()
                    } else {
                        state.ringLimitReachedVibration()
                    }
                }
            
            RoundedRectangle(cornerRadius: 5)
                .frame(width: width, height: height)
                .foregroundColor(ring == .inner ? Color(.smartBlue) : Color(.background).opacity(0))
                .overlay {
                    Text("50m")
                        .foregroundColor(ring == .inner ? .white : Color(.smartBlue))
                }
                .animation(.easeInOut, value: ring)
                .onTapGesture {
                    if innerEnabled {
                        innerClicked()
                    } else {
                        state.ringLimitReachedVibration()
                    }
                }
            
            RoundedRectangle(cornerRadius: 5)
                .frame(width: width, height: height)
                .foregroundColor(ring == .nearby ? Color(.smartBlue) : Color(.background).opacity(0))
                .overlay {
                    Text("Nah")
                        .foregroundColor(ring == .nearby ? .white : Color(.smartBlue))
                }
                .animation(.easeInOut, value: ring)
                .onTapGesture {
                    if nearbyEnabled {
                        nearbyClicked()
                    } else {
                        state.ringLimitReachedVibration()
                    }
                }
        }
        .background(
            RoundedRectangle(cornerRadius: 5)
                .stroke(Color(.smartBlue))
                .foregroundColor(Color(.background))
        )
    }
}

struct FloatingStateView: View {
    @State private var ring: RingStatus = .nearby
    
    var body: some View {
        FloatingRingMenu(ring: $ring) {
            ring = .nearby
        } innerClicked: {
            ring = .inner
        } middleClicked: {
            ring = .middle
        } outerClicked: {
            ring = .outer
        }
    }
}

struct FloatingRingMenu_Previews: PreviewProvider {
    static var previews: some View {
        FloatingStateView()
        
        FloatingStateView()
            .preferredColorScheme(.dark)
    }
}
