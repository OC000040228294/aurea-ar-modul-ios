//
//  DevButton.swift
//  CityPad
//
//  Created by lichen on 22.03.23.
//

import SwiftUI

struct DevButton: View {
    let mainState: MainState
    var body: some View {
        Button {
            mainState.toggleDevMode()
        } label: {
            Image(systemName: "cube.fill")
        }
        .padding(.horizontal)
    }
}

struct DevButton_Previews: PreviewProvider {
    static var previews: some View {
        DevButton(mainState: MainState())
    }
}
