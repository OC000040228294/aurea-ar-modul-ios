//
//  OnboardingPage3.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct OnboardingPage3: View {
    @AppStorage("onboarding") var isOnboardingViewActive = true
    
    @Binding var permissions: Bool
    @State var canContinue = false
    
    var body: some View {
        ZStack {
            VStack {
                Text("Berechtigungen")
                    .font(.title)
                    .padding()
                
                Spacer()
                
                Text("Für das Aurea AR-Erlebnis, benötigt die App deine Erlaubnis auf deinen Standort und die Kamera zuzugreifen. Es werden keine Daten gespeichert oder weitergegeben.")
                    .padding(.horizontal)
                
                Spacer()
                
                Button {
                    permissions = true
                    canContinue = true
                } label: {
                    Text("Berechtigungen abfragen")
                }
                .padding()
                
                Spacer()
                
                Button {
                    if canContinue {
                        isOnboardingViewActive = false
                    } else {
                        canContinue = true
                        permissions = true
                    }
                } label: {
                  Image(systemName: "chevron.right.circle")
                        .resizable()
                        .foregroundColor(
                            canContinue ? Color("SmartBlue") : .gray
                        )
                        .frame(width: 40, height: 40)
                }
                .padding(32)
                .modifier(RightBoundModifier())
            }
        }
    }
}

struct OnboardingPage3_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPage3(permissions: .constant(false))
    }
}
