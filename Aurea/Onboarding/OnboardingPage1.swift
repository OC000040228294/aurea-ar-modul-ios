//
//  OnboardingPage1.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct OnboardingPage1: View {
    @Binding var page: Int
    
    var body: some View {
        GeometryReader {geometry in
            let w: Double = geometry.size.width
            let h: Double = geometry.size.height
            let wh: Double = w < h ? w : h
            
            VStack {
                Text("Entdecke Aurea")
                    .font(.title)
                    .padding()
                
                Spacer()
                
                CompassView(rotation: 0)
                    .frame(width: wh * 0.8)
                
                Spacer()
                
                Text("Entdecke das Aurea Gebiet und seine Sensoren. Zeige in die Richtung eines oder mehrerer Sensoren auf dem Kompass und hebe dein Smartphone an.")
                    .padding(.horizontal)
                
                Spacer()
                
                Button {
                    page += 1
                } label: {
                  Image(systemName: "chevron.right.circle")
                        .resizable()
                        .foregroundColor(Color("SmartBlue"))
                        .frame(width: 40, height: 40)
                }
                .padding(32)
                .modifier(RightBoundModifier())
            }            
        }
        
    }
}

struct OnboardingPage1_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPage1(page: .constant(0))
    }
}
