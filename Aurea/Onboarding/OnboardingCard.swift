//
//  OnboardingCard.swift
//  CityPad
//
//  Created by lichen on 06.10.22.
//

import Foundation

struct OnboardingCard: Identifiable {
    var id = UUID()
    var title: String
    var description: String
}

let testCards: [OnboardingCard] = [
    OnboardingCard( title: "Choose A Tasty", description: "When you order Eat Street , we'll hook you up with exclusive  coupons."),

    OnboardingCard( title: "Discover Places", description: "We make it simple to find the food you crave. Enter your address and let"),
    
    OnboardingCard( title: "Pick Up Or", description: "We make food ordering fast , simple and free - no matter if you order"),
]
