//
//  OnboardingView.swift
//  CityPad
//
//  Created by lichen on 06.10.22.
//

import SwiftUI

struct OnboardingView: View {
    @AppStorage("onboarding") var isOnboardingViewActive = true
    @State var selectedPage = 0
    @Binding var permissions: Bool
    
    var body: some View {
        ZStack{
            Color("WhiteBlack")
                .edgesIgnoringSafeArea(.all)
                        
            
            TabView(selection: $selectedPage) {
                ForEach(0..<3) {index in
                    switch index {
                    case 0:
                        OnboardingPage1(page: $selectedPage)
                            .tag(index)
                    case 1:
                        OnboardingPage2(page: $selectedPage)
                            .tag(index)
                    default:
                        OnboardingPage3(permissions: $permissions)
                            .tag(index)
                    }
                }
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(permissions: .constant(false))
    }
}
