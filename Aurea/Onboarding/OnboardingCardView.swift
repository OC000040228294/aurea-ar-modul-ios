//
//  OnboardingCardView.swift
//  CityPad
//
//  Created by lichen on 06.10.22.
//

import SwiftUI

struct OnboardingCardView: View {
    let card: OnboardingCard
    
    var body: some View {
        VStack{
            Text(card.title)
                .font(.system(size: 40))
                .fontWeight(.bold)
                .foregroundColor(Color("Background"))
            
            Text(card.description)
                .fontWeight(.light)
                .multilineTextAlignment(.center)
                .font(.system(size: 24))
                .foregroundColor(Color("Background"))
                .frame(width: 335, height: 100)
                .padding()
        
        }
        .padding()
        .offset(x: 0, y: 250)
    }
    }


struct OnboardingCardView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingCardView(card: testCards[1])
            .previewLayout(.sizeThatFits)
            .preferredColorScheme(.light)
        
        OnboardingCardView(card: testCards[1])
            .previewLayout(.sizeThatFits)
            .preferredColorScheme(.dark)
    }
}
