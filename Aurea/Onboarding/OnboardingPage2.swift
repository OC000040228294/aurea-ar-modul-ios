//
//  OnboardingPage2.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

struct OnboardingPage2: View {
    @Binding var page: Int
    
    var body: some View {
        GeometryReader {geometry in
            
            VStack {
                Text("Entdecke die Aurea Sensoren in Augmented Reality")
                    .font(.title)
                    .padding()
                    
                Spacer()
            
                Image("intro_ar_view")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 100.0, alignment: .center)
                
                Spacer()
        
                Text("Schaue dir aktuelle Werte und Informationen zu den einzelnen Sensoren in Augmented Reality an.")
                    .padding(.horizontal)
                
                Spacer()
                
                Button {
                    page += 1
                } label: {
                  Image(systemName: "chevron.right.circle")
                        .resizable()
                        .foregroundColor(Color("SmartBlue"))
                        .frame(width: 40, height: 40)
                }
                .padding(32)
                .modifier(RightBoundModifier())
            }
        }
        
    }
}

struct OnboardingPage2_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPage2(page: .constant(0))
    }
}
