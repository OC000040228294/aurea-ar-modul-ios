//
//  Util.swift
//  CityPad
//
//  Created by lichen on 30.05.22.
//

import Foundation
import SwiftUI
import CoreLocation

let MAX_DISTANCE_FROM_MAIN_AREA = 500.0
let hoverOpacity = 1.0

let centerOfMainArea = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 51.30124136846982, longitude: 9.5001839602235), altitude: 150, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now) // set to auebad location for now

func formatDate(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateStyle = .long
    return formatter.string(for: date) ?? ""
}

func getOccupancyIcon(_ capacity: PoolCapacity) -> ImageResource {
    switch capacity {
    case .LOW:
        return .poolOccupancy25
    case .MEDIUM:
        return .poolOccupancy50
    case .HIGH:
        return .poolOccupancy100
    default:
        return .poolOccupancy25
    }
}

func inMainArea(position: CLLocation) -> Bool {
    let distance = position.distance(from: centerOfMainArea)
    
    return distance <= MAX_DISTANCE_FROM_MAIN_AREA
}

enum ImageString: String {
    case Attraction = "ic_attraction"
    case TrafficLight = "ic_traffic"
    case Compass = "compassv2"
    case Food = "food"
    case Freetime = "freetime"
    case Cluster = "ic_cluster"
    case Hand = "ic_hand"
    case Home = "ic_home"
    case LeftArrow = "ic_left_arrow"
    case RightArrow = "ic_right_arrow"
    case Near = "ic_near"
    case Money = "money"
    case Sight = "sight"
    case Closed = "ic_close"
    case Dashboard = "ic_dashboard"
    case RadarHome = "ic_radar_home"
    case Notification = "ic_notification"
    case Placed = "cube.fill"
    case PlantSensor = "tree"
    case TrafficSensor = "vehicle_large"
    case CO2 = "co2"
    case Waterlevel = "waterlevel"
    case SwimmingPool = "pool_indoor"
    case Info = "info.circle"
    case Parking = "parking"
    case Lantern = "lantern"
}

func aureaPosition() -> CLLocation {
    return CLLocation(coordinate: CLLocationCoordinate2D(latitude: 51.301838114, longitude: 9.499663905), altitude: 150.0, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now)
}
