//
//  Caching#.swift
//  CityPad
//
//  Created by lichen on 02.03.23.
//

import Foundation


let plantsPath = FileManager.documentsDirectory.appendingPathComponent("cachedPlants")
let co2sPath = FileManager.documentsDirectory.appendingPathComponent("cachedCO2s")
let levelsPath = FileManager.documentsDirectory.appendingPathComponent("cachedLevels")
let swimmingPath = FileManager.documentsDirectory.appendingPathComponent("cachedSwimming")
let lanternPath = FileManager.documentsDirectory.appendingPathComponent("cachedLantern")
let trafficPath = FileManager.documentsDirectory.appendingPathComponent("cachedTraffic")
let parkingPath = FileManager.documentsDirectory.appendingPathComponent("cachedParking")

extension FileManager {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

/// Clears all cached data.
func clearCaches() {
    do {
        let file = FileManager()

        if file.fileExists(atPath: plantsPath.path) {
            try file.removeItem(atPath: plantsPath.path)
        }
        
        if file.fileExists(atPath: co2sPath.path) {
            try file.removeItem(atPath: co2sPath.path)
        }
        
        if file.fileExists(atPath: levelsPath.path) {
            try file.removeItem(atPath: levelsPath.path)
        }
        
        if file.fileExists(atPath: parkingPath.path) {
            try file.removeItem(atPath: parkingPath.path)
        }
        
        if file.fileExists(atPath: swimmingPath.path) {
            try file.removeItem(atPath: swimmingPath.path)
        }
        
        if file.fileExists(atPath: lanternPath.path) {
            try file.removeItem(atPath: lanternPath.path)
        }
        
        if file.fileExists(atPath: trafficPath.path) {
            try file.removeItem(atPath: trafficPath.path)
        }
        
        print("cleared")
    } catch {
        print("Unable to clear caches.")
    }
}

func cacheCO2(co2s: [CO2Sensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(co2s)
        
        try encodedSensors.write(to: co2sPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cacheWaterlevels(levels: [WaterlevelSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(levels)
        
        try encodedSensors.write(to: levelsPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cachePlants(plants: [PlantSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(plants)
        
        try encodedSensors.write(to: plantsPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cacheSwimming(swimming: [SwimmingPoolSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(swimming)
        
        try encodedSensors.write(to: swimmingPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cacheTraffic(traffic: [TrafficSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(traffic)
        
        try encodedSensors.write(to: trafficPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cacheLantern(lantern: [LanternSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(lantern)
        
        try encodedSensors.write(to: lanternPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

func cacheParking(parking: [ParkingSensor]) {
    do {
        let encodedSensors = try JSONEncoder().encode(parking)
        
        try encodedSensors.write(to: parkingPath, options: [.atomicWrite, .completeFileProtection])
    } catch {
        print("Unable to save data.")
    }
}

/// Loads all cached data.
/// - Returns: An array of all cached sensors.
func loadCachedData() -> [any Sensor] {
    let plants = loadPlants()
    let swimming = loadSwimming()
    let traffic = loadTraffic()
    let parking = loadParking()
    let lanterns = loadLantern()
    let co2s = loadCO2()
    let levels = loadLevels()
    
    var sensors: [any Sensor] = []
    sensors.append(contentsOf: plants)
    sensors.append(contentsOf: parking)
    sensors.append(contentsOf: lanterns)
    sensors.append(contentsOf: swimming)
    sensors.append(contentsOf: traffic)
    sensors.append(contentsOf: co2s)
    sensors.append(contentsOf: levels)
    
    return sensors
}

func loadCO2() -> [CO2Sensor] {
    guard FileManager().fileExists(atPath: co2sPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: co2sPath)
        
        let decodedSensors = try JSONDecoder().decode([CO2Sensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}

func loadLevels() -> [WaterlevelSensor] {
    guard FileManager().fileExists(atPath: levelsPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: levelsPath)
        
        let decodedSensors = try JSONDecoder().decode([WaterlevelSensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}

func loadPlants() -> [PlantSensor] {
    guard FileManager().fileExists(atPath: plantsPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: plantsPath)
        
        let decodedSensors = try JSONDecoder().decode([PlantSensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}

func loadSwimming() -> [SwimmingPoolSensor] {
    guard FileManager().fileExists(atPath: swimmingPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: swimmingPath)
        
        let decodedSensors = try JSONDecoder().decode([SwimmingPoolSensor].self, from: data)
        
        let mapped = decodedSensors
        
        return mapped
    } catch{
        print(error)
    }
    
    return []
}

func loadTraffic() -> [TrafficSensor] {
    guard FileManager().fileExists(atPath: trafficPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: trafficPath)
        
        let decodedSensors = try JSONDecoder().decode([TrafficSensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}

func loadParking() -> [ParkingSensor] {
    guard FileManager().fileExists(atPath: parkingPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: parkingPath)
        
        let decodedSensors = try JSONDecoder().decode([ParkingSensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}

func loadLantern() -> [LanternSensor] {
    guard FileManager().fileExists(atPath: lanternPath.path) else {return []}
    
    do {
        let data = try Data(contentsOf: lanternPath)
        
        let decodedSensors = try JSONDecoder().decode([LanternSensor].self, from: data)
        
        return decodedSensors
    } catch{
        print(error)
    }
    
    return []
}
