//
//  UseCaseInfo.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import Foundation

func getTargetedSensorTypeInAr(index: Int, sensors: [any Sensor]) -> SensorType {
    if sensors.isEmpty {
        return SensorType.PLANT
    } else {
        if index < 0 {
            return sensors[0].type
        } else if index >= sensors.count {
            return sensors.last!.type
        } else {
            return sensors[index].type
        }
    }
}

/// Urls to use-case description of the aurea sensors by type
func useCaseLink(type: SensorType) -> String {
    switch type {
    case .PLANT:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-bodenfeuchte-fuer-alleebaeume.php"
    case.LANTERN:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-strassenlicht-nach-bedarf.php"
    case .PARKING:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-parkplaetze-fuer-menschen-mit-behinderung.php"
    case .SWIMMING:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-besucher-im-auebad.php"
    case .TRAFFIC:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-verkehrszaehlung.php"
    case .CO2:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-luftqualitaet-im-auebad.php"
    case .WATERLEVEL:
        return "https://www.kassel.de/einrichtungen/aurea/sensoren-am-auepark/sensor-wasserstand-fulda.php"
    default:
        return ""
    }
}

func useCaseInfo(type: SensorType) -> String {
    switch type {
    case .PLANT:
        return getUseCaseInfo("TREE")
    case.LANTERN:
        return getUseCaseInfo("LIGHT")
    case .PARKING:
        return getUseCaseInfo("PARKING")
    case .SWIMMING:
        return getUseCaseInfo("POOL")
    case .TRAFFIC:
        return getUseCaseInfo("TRAFFIC")
    case .CO2:
        return getUseCaseInfo("CO2")
    case .WATERLEVEL:
        return getUseCaseInfo("WATERLEVEL")
    default:
        return ""
    }
}

func getUseCaseInfo(_ fileName: String) -> String {
    if let filepath = Bundle.main.path(forResource: fileName, ofType: "html") {
        do {
            let contents = try String(contentsOfFile: filepath)
        
            return contents
        } catch {
            return "<h1>Keine Informationen</h1>"
        }
    }
    
    return "<h1>Keine Informationen</h1>"
}
