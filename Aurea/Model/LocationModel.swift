//
//  LocationModel.swift
//  CityPad
//
//  Created by Sebastian Lange on 21.01.22.
//

import MapKit
import CityPadFramework


final class LocationModel: NSObject, ObservableObject,
                           CLLocationManagerDelegate, LocationModelProtocol {
    
    @Published var heading: Double = .zero
    var headingPublished: Published<Double> { _heading }
    var headingPublisher: Published<Double>.Publisher { $heading }
    
    @Published private(set) var location: CLLocation = CLLocation()
    var locationPublished: Published<CLLocation> { _location }
    var locationPublisher: Published<CLLocation>.Publisher { $location }
    
    @Published var authorizationStatus: CLAuthorizationStatus = CLAuthorizationStatus.notDetermined
    var authorizationStatusPublished: Published<CLAuthorizationStatus> { _authorizationStatus }
    var authorizationStatusPublisher: Published<CLAuthorizationStatus>.Publisher { $authorizationStatus }
    
    var locationManager: CLLocationManager?
    
    static let shared: LocationModel = LocationModel()
    
    override private init() {
        super.init()
        checkLocationServiceIsEnabled()
    }
    
    func checkLocationServiceIsEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.startUpdatingLocation()
            locationManager?.startUpdatingHeading()
        } else {
            print("")
        }
    }
    
    func checkLocationAuthorization() {
        guard let locationManager = locationManager else { return }
        
            authorizationStatus = locationManager.authorizationStatus
            
            switch locationManager.authorizationStatus {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted:
                break
            case .denied:
                break
            case .authorizedAlways:
                break
            case .authorizedWhenInUse:
                break
            @unknown default:
                break
            }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first!
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        heading = -1 * newHeading.magneticHeading
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       if let error = error as? CLError, error.code == .denied {
          // Location updates are not authorized.
          manager.stopUpdatingLocation()
          return
       }
       // Notify the user of any errors.
        print("Error on location...")
    }
}
