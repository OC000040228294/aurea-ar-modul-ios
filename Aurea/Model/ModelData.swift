//
//  ModelData.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import Foundation
import Combine

/// Singleton Model for the sensor data
/// It is used to fetch the sensor data.
/// @param sensors: The fetched sensors
/// @param shared: The shared instance of the model data
@MainActor
final class ModelData: ObservableObject {
    @Published var sensors: [any Sensor] = loadSensors()
    
    static let shared = ModelData()
    
    /// Fetches all sensors
    func refreshSensors() async {
        var copy = self.sensors
        let fetched = await fetchAllSensors(sensors: &copy)
        sensors = fetched
    }
}

/// Fetches all sensors defined in the sensors.json file
func loadSensors() -> [any Sensor] {
    
    var allSensors: [SensorStruct] = []
    
    let cachedSensors: [any Sensor] = loadCachedData()
    var loadedSensors: [SensorStruct] = []
    loadedSensors = load("sensors.json")
    allSensors.append(contentsOf: loadedSensors)
    
    // DEBUGGING: Adds sensors from test.json
//    var testSensors: [SensorStruct] = []
//    testSensors = load("test.json")
//    allSensors.append(contentsOf: testSensors)
    
    if cachedSensors.isEmpty || loadedSensors.count != cachedSensors.count {
        var sensors: [any Sensor] = []
        
        for loadedSensor in allSensors {
            switch loadedSensor.type {
            case .PLANT:
                let plantSensor = PlantSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(plantSensor)
            case .TRAFFIC:
                let trafficSensor = TrafficSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(trafficSensor)
            case .SWIMMING:
                let swimming = SwimmingPoolSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(swimming)
            case .PARKING:
                let parking = ParkingSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(parking)
            case .LANTERN:
                let lantern = LanternSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(lantern)
            case .CO2:
                let co2 = CO2Sensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(co2)
            case .WATERLEVEL:
                let level = WaterlevelSensor(id: loadedSensor.id, name: loadedSensor.name, lat: loadedSensor.lat, lng: loadedSensor.lng, alt: loadedSensor.alt, type: loadedSensor.type, apiLink: loadedSensor.apiLink, distance: loadedSensor.distance, locationLink: loadedSensor.locationLink)
                sensors.append(level)
            default:
                print("unknown sensor type found: \(loadedSensor.type)")
            }
        }
        
        return sensors
    }
    
    return cachedSensors
}

func load<T: Decodable>(_ filename: String) -> T {

    let data: Data


    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)

    else {

        fatalError("Couldn't find \(filename) in main bundle.")

    }


    do {

        data = try Data(contentsOf: file)

    } catch {

        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")

    }


    do {

        let decoder = JSONDecoder()

        return try decoder.decode(T.self, from: data)

    } catch {

        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")

    }

}
