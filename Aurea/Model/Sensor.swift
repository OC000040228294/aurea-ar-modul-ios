//
//  Sensor.swift
//  CityPad
//
//  Created by lichen on 20.09.22.
//

import Foundation
import CoreLocation
import SwiftUI
import CityPadFramework

/// SensorType is an enum that defines the different text of types of sensors
enum SensorType: String, CaseIterable, Codable {
    case NONE = "Unbekannt"
    case PLANT = "Pflanze"
    case SWIMMING = "Schwimmbad"
    case TRAFFIC = "Verkehr"
    case PARKING = "Parken"
    case LANTERN = "Laterne"
    case CO2 = "CO2"
    case WATERLEVEL = "Wasserpegel"
}

/// Extension of Sensor that defines the location property of a sensor
extension Sensor {
    var location: CLLocation {
        CLLocation(
            coordinate: CLLocationCoordinate2D(
                latitude: lat,
                longitude: lng
            ),
            altitude: alt,
            horizontalAccuracy: 0.0,
            verticalAccuracy: 0.0,
            timestamp: Date.now
        )
    }
}

extension Sensor {
    static func appendDataToId(sensor: any Sensor) -> String {
        switch sensor.type {
        case .PLANT:
            if let plantSensor = sensor as? PlantSensor {
                return "\(plantSensor.plantData)"
            }
        case .TRAFFIC:
            if let trafficSensor = sensor as? TrafficSensor {
                return "\(trafficSensor.trafficData)"
            }
        case .SWIMMING:
            if let swimming = sensor as? SwimmingPoolSensor {
                return "\(swimming.swimmingPoolData)"
            }
        case .PARKING:
            if let parking = sensor as? ParkingSensor {
                return "\(parking.parkingData)"
            }
        case .CO2:
            if let co2 = sensor as? CO2Sensor {
                return "\(co2.data)"
            }
        case .WATERLEVEL:
            if let level = sensor as? WaterlevelSensor {
                return "\(level.data)"
            }
        default:
            return ""
        }
        
        return ""
    }
}

/// Wrapper functions for FityPad Framework
extension Sensor {
    func toLibSensor() -> LibSensor {
        LibSensor(id: id, name: name, lat: lat, lng: lng, alt: alt, distance: distance)
    }
    
    func toArItem(current: Int, max: Int, maxOpacity: Bool) -> ArItem {
        ArItem(id: id, view: AnyView(ARHoverView(sensor: self, current: current, max: max, maxOpacity: maxOpacity)), lat: lat, lng: lng, alt: alt, distance: distance)
    }
}

/// SensorStruct is a struct that defines the properties of a sensor
struct SensorStruct: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
}

struct Container: Identifiable, Equatable {
    static func == (lhs: Container, rhs: Container) -> Bool {
        rhs.wrapped.id == lhs.wrapped.id
    }
    
    var wrapped: any Sensor
    //for convenience
    var id: String {wrapped.id}
}

protocol Sensor: Codable, Identifiable {
    var id: String { get set }
    var name: String { get set }
    var lat: Double { get set }
    var lng: Double { get set }
    var alt: Double { get set }
    var type: SensorType { get set }
    var apiLink: String { get set }
    var distance: Double { get set }
    var locationLink: String { get set }    
}

let sensorExampleData: [any Sensor] = [
    PlantSensor(id: "0", name: "plant", lat: 0, lng: 0, alt: 0, type: SensorType.PLANT, apiLink: "", distance: 50, locationLink: ""),
    TrafficSensor(id: "1", name: "traffic", lat: 0, lng: 0, alt: 0, type: SensorType.TRAFFIC, apiLink: "", distance: 100, locationLink: ""),
    SwimmingPoolSensor(id: "2", name: "swim", lat: 0, lng: 0, alt: 0, type: SensorType.SWIMMING, apiLink: "", distance: 150, locationLink: ""),
    ParkingSensor(id: "3", name: "parking", lat: 0, lng: 0, alt: 0, type: SensorType.PARKING, apiLink: "", distance: 200, locationLink: ""),
    LanternSensor(id: "4", name: "lantern", lat: 0, lng: 0, alt: 0, type: SensorType.LANTERN, apiLink: "", distance: 250, locationLink: ""),
]
