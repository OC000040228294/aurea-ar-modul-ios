//
//  PlantSensor.swift
//  CityPad
//
//  Created by lichen on 27.10.22.
//

import Foundation

struct PlantSensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var plantData: PlantData = PlantData(moisture1: .low, moisture2: .low, moisture3: .low)
}
