//
//  ParkingSensor.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import Foundation

struct ParkingSensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var parkingData: ParkingData = ParkingData(free1: false, free2: false)
}
