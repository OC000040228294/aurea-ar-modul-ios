//
//  SwimmingPoolSensor.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import Foundation

struct SwimmingPoolSensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var swimmingPoolData: SwimmingPoolData = SwimmingPoolData(sauna: 0, outdoor: 0, indoor: 0, sauna_capacity: .HIGH, outdoor_capacity: .HIGH, indoor_capacity: .HIGH)
}
