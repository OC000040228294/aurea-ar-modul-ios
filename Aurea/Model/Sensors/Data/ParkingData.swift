//
//  ParkingData.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import Foundation

struct ParkingData: Codable {
    var free1: Bool
    var free2: Bool
}
