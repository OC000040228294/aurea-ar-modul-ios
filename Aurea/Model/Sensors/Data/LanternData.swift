//
//  LanternData.swift
//  Aurea
//
//  Created by lichen on 15.06.23.
//

import Foundation

struct LanternData: Codable {
    let brightness: Int
}
