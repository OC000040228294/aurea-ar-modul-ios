//
//  PlantData.swift
//  CityPad
//
//  Created by lichen on 27.10.22.
//

import Foundation

enum PlantMoisture: String, Codable {
    case low = "low", medium = "medium", high = "high"
}

struct PlantData: Codable {
    //moisture level 20cm
    var moisture1: PlantMoisture
    
    //moisture level 50cm
    var moisture2: PlantMoisture
    
    //moisture level 80cm
    var moisture3: PlantMoisture
}
