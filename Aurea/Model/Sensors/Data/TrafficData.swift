//
//  TrafficData.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import Foundation

struct TrafficData: Codable {
    let hugeSum: Int
    let largeSum: Int
    let mediumSum: Int
    let smallSum: Int
}
