//
//  SwimmingPoolData.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import Foundation

enum PoolCapacity: Codable {
    case LOW
    case MEDIUM
    case HIGH
    case UNKNOWN
}

struct SwimmingPoolData: Codable {
    var sauna: Int
    var outdoor: Int
    var indoor: Int
    var sauna_capacity: PoolCapacity
    var outdoor_capacity: PoolCapacity
    var indoor_capacity: PoolCapacity
}
