//
//  CO2Data.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import Foundation

struct CO2Data: Codable {
    let ppm: String
    let classification: CO2Classification
}

enum CO2Classification: String, Codable {
    case normal = "Normal", noticeable = "Noticeable", unacceptable = "Unacceptable"
}
