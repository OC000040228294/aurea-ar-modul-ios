//
//  WaterlevelData.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import Foundation

struct WaterlevelData: Codable {
    let level: String
}
