//
//  LanternSensor.swift
//  CityPad
//
//  Created by lichen on 05.01.23.
//

import Foundation

struct LanternSensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var data: LanternData = LanternData(brightness: 0)
}
