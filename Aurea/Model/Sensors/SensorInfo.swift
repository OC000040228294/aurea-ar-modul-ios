//
//  SensorInfo.swift
//  CityPad
//
//  Created by lichen on 16.01.23.
//

import Foundation

struct SensorInfo {
    var url: String
    var info: String
}
