//
//  CO2Sensor.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import Foundation

struct CO2Sensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var data: CO2Data = CO2Data(ppm: "0", classification: .normal)
}
