//
//  TrafficSensor.swift
//  CityPad
//
//  Created by lichen on 26.09.22.
//

import Foundation

struct TrafficSensor: Sensor {
    var id: String
    
    var name: String
    
    var lat: Double
    
    var lng: Double
    
    var alt: Double
    
    var type: SensorType
    
    var apiLink: String
    
    var distance: Double
    
    var locationLink: String
    
    var trafficData: TrafficData = TrafficData(hugeSum: 0, largeSum: 0, mediumSum: 0, smallSum: 0)
}
