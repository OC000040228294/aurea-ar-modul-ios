//
//  SettingsHome.swift
//  CityPad
//
//  Created by lichen on 05.12.22.
//

import SwiftUI

/// SettingsHome is the main view for the settings tab.
struct SettingsHome: View {
    @AppStorage("onboarding") var isOnboardingViewActive = true
    @AppStorage("useSounds") var useSounds = true
    @AppStorage("useVibrations") var useVibrations = true
    @AppStorage("opacity") var opacity = hoverOpacity
    
    var body: some View {
        NavigationView {
            VStack {
                VStack(spacing: 32) {
                    Toggle(isOn: $useSounds) {
                        VStack(alignment: .leading) {
                            Text("Sound")
                                .bold()
                            Text("Geräusche als Feedback bei Aktionen")
                                .font(.caption)
                        }
                    }
                    Toggle(isOn: $useVibrations) {
                        VStack(alignment: .leading) {
                            Text("Vibration")
                                .bold()
                            Text("Vibration als Feedback bei Aktionen")
                                .font(.caption)
                        }
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Transparenz der AR Kacheln: \(String(format: "%.0f", opacity*100))%")
                            .bold()
                        
                        Slider(
                            value: $opacity,
                            in: 0.1...1.0
                        ) {
                            Text("Transparenz")
                        } minimumValueLabel: {
                            Text("10%")
                        } maximumValueLabel: {
                            Text("100%")
                        }
                    }
                }
                .padding()
                .padding(.horizontal)
                .navigationTitle("Einstellungen")
                
                Spacer()
                
                Text("App Version: \(Bundle.main.releaseVersionNumber)")
                
                Spacer()
                
            }
        }
        .ignoresSafeArea()
        .background(ignoresSafeAreaEdges: .all)
    }
}

struct SettingsPreviewHost: View {
    @State private var selectedItem = 0

    var body: some View {
        TabView(selection: $selectedItem) {
            SettingsHome()
                .tabItem {
                    Label("Einstellungen", systemImage: "gearshape.fill")
                }
                .tag(3)
        }
    }
}

struct SettingsHome_Previews: PreviewProvider {
    
    static var previews: some View {
        SettingsPreviewHost()
    }
}
