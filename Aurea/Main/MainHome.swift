//
//  MainHome.swift
//  CityPad
//
//  Created by lichen on 05.12.22.
//

import SwiftUI
import CityPadFramework

/// MainHome is the main feature view of the app. 
///
/// Displays the AR view or the compass view depending on the tilt of the phone.
struct MainHome: View {
    @Environment(\.scenePhase) var scenePhase
    
    @StateObject private var motion: MotionManager = MotionManager.shared
    @StateObject private var model = ModelData.shared
    @StateObject private var state = MainState.shared
    @ObservedObject private var location = LocationModel.shared
    @StateObject private var compassState = CompassState(model: ModelData.shared, mainState: MainState.shared)
     
    @State private var recurringRefreshTask: Task<Void, Never>?
    
    var swap: Bool {
        motion.swapToAr
    }
    
    var body: some View {
        if CityPadUtils.noLocationPermission(status: location.authorizationStatus) {
            NoLocationCard()
        } else {
            GeometryReader {geometry in
                if !swap && !state.showPopup {
                    CompassHome()
                        .environmentObject(compassState)
                } else {
                    ARHome()
                        .popover(isPresented: $state.showPopup) {
                            if let info = state.sensorInfo {
                                VStack {
                                    SensorInfoView(type: info)
                                        .padding()
                                    
                                    Spacer()
                                    
                                    Button {
                                        state.showPopup = false
                                    } label: {
                                        Text("Schließen")
                                    }
                                    .padding()
                                }
                            } else {
                                Text("Keine Information")
                            }
                        }
                }
            }
            .environmentObject(model)
            .environmentObject(state)
            .onChange(of: scenePhase, perform: { value in
                switch value {
                case .active:
                    refreshSensors()
                case .background:
                    cancelTask()
                default: break
                }
            })
        }
    }
    
    private func refreshSensors() {
        recurringRefreshTask?.cancel()
        recurringRefreshTask = Task {
            await model.refreshSensors()
            do {
                try await Task.sleep(nanoseconds: API_DATA_REFRESH_INTERVAL)
            } catch {
            }
            guard !Task.isCancelled else { return }
            refreshSensors()
        }
    }

    private func cancelTask() {
        recurringRefreshTask?.cancel()
    }
}

struct MainHome_Previews: PreviewProvider {
    static var previews: some View {
        MainHome()
            .environmentObject(ModelData.shared)
            .environmentObject(MainState.shared)
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
            .environmentObject(ArState(locationModel: LocationModel.shared, currentNearbyThresholdMeters: currentNearbyThresholdMeters))
        
        MainHome()
            .environmentObject(ModelData.shared)
            .environmentObject(MainState.shared)
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
            .environmentObject(ArState(locationModel: LocationModel.shared, currentNearbyThresholdMeters: currentNearbyThresholdMeters))
    }
}
