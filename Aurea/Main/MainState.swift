//
//  MainState.swift
//  CityPad
//
//  Created by lichen on 22.08.22.
//

import Foundation
import CoreLocation
import CityPadFramework

/// MainState is a singleton class that holds the main state of the app.
/// @property targetList: list of targeted sensor ids
/// @property fixedLocation: location to use if a nearby sensor is targeted
/// @property nearbyLocation: location of the nearby sensor
/// @property useFixedNearbyLocation: whether to use the fixed location
@MainActor
final class MainState: ObservableObject {
    static let shared = MainState()

    var targetList: [String] = []
    var fixedLocation: CLLocation = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0), altitude: 0.0, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now)
    var nearbyLocation: CLLocation = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0), altitude: 0.0, horizontalAccuracy: 0.0, verticalAccuracy: 0.0, timestamp: Date.now)
    var useFixedNearbyLocation = false
    @Published var selectedSensors: [any Sensor]? = nil
    @Published var devModeSimulateLocation = false
    @Published var sensorInfo: SensorType? = nil
    @Published var showPopup: Bool = false
    
    func setSensorInfo(type: SensorType) {
        sensorInfo = type
        showPopup = true
    }
    
    func toggleDevMode() {
        devModeSimulateLocation = !devModeSimulateLocation
    }
    
    func targetSensors(sensors: [any Sensor]) {
        // called if user targets sensors in compass
        if MotionManager.shared.swapToAr {
            return
        }
        
        DispatchQueue.main.async {
            self.targetList = sensors.map{
                $0.id
            }
        }
    }
    
    func untargetSensors() {
        // called if user untargets sensors in compass
        if MotionManager.shared.swapToAr {
            return
        }
        
        targetList = []
    }
}
