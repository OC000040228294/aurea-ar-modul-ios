//
//  TabState.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import Foundation

enum TabState {
    case main
    case other
}

class TabStateManager: ObservableObject {
    static let shared = TabStateManager()
    
    @Published var state: TabState = .main
    
    func setMainState() {
        state = .main
    }
    
    func setOtherState() {
        state = .other
    }
}
