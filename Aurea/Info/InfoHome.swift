//
//  InfoHome.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import SwiftUI
import WebKit

struct InfoHome: View {
    @EnvironmentObject var state: MainState
    
    var body: some View {
        ScrollView(.vertical) {
            AureaInfoView()
        }
    }
}

struct InfoHome_Previews: PreviewProvider {
    static var previews: some View {
        InfoHome()
    }
}

struct InfoWebView: UIViewRepresentable {
    typealias UIViewType = WKWebView
    
    let webView: WKWebView
    
    init() {
        webView = WKWebView(frame: .zero)
        webView.load(URLRequest(url: URL(string: "https://www.kvv.de/")!))
    }
    
    func makeUIView(context: Context) -> WKWebView {
        webView
    }
    func updateUIView(_ uiView: WKWebView, context: Context) {
    }
}
