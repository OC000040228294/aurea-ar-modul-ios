//
//  AureaInfoView.swift
//  CityPad
//
//  Created by lichen on 10.02.23.
//

import SwiftUI

/// AureaInfoView shows information about the project aurea.
struct AureaInfoView: View {
    let text: String = """
AUREA ist unser Kasseler Sensorik‐Reallabor am Auedamm. Hier erproben wir smarte Techniken für die ganze Stadt. Der Name setzt sich aus den Wörtern „Aue“ und „Area“, dem englischen Begriff für „Gebiet“, zusammen.

Derzeit setzen wir Sensoren rund um das Auebad ein, die zum Beispiel den Pegel der Fulda, die Feuchte im Wurzelbereich der Bäume sowie die Zahl und Größe der Fahrzeuge am Auedamm messen.

Mit dieser APP haben Sie die spielerische Möglichkeit die Echtzeitdaten der Sensoriken im realen Livebild anzuzeigen (Augmented Reality).  Dazu entdecken Sie die Sensoren einfach per Kompass.

Viel Spaß.
"""
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text("Willkommen im AUREA")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 32)
                    .padding(.vertical, 16)
                    .modifier(CenterBoundModifier())
                
                Text(text)
                    .font(.title3)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 32)
                
                Image("logo_kassel")
                    .resizable()
                    .scaledToFit()
                    .padding(.horizontal, 32)
                    .frame(width: 250)
                    .modifier(CenterBoundModifier())
                
                VStack(spacing: 16) {
                    Text("Weitere Informationen")
                        .font(.largeTitle.bold())
                        .multilineTextAlignment(.center)
                        .modifier(CenterBoundModifier())
                    
                    Link("Datenschutz", destination: URL(string: "https://www.kvvks.de/datenschutz/#c25883")!)
                        .padding(.horizontal)
                        .modifier(LeftBoundModifier())
                    Link("Impressum", destination: URL(string: "https://www.kvvks.de/impressum-1/")!)
                        .padding(.horizontal)
                        .modifier(LeftBoundModifier())
                }
                .padding()
                
                Spacer()
            }
        }
    }
}

struct AureaInfoView_Previews: PreviewProvider {
    static var previews: some View {
        AureaInfoView()
    }
}
