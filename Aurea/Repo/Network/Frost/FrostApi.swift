//
//  FrostApi.swift
//  CityPad
//
//  Created by lichen on 27.09.22.
//

import Foundation
import Alamofire

/// FrostApi is responsible for fetching sensor data from the KVV frost api.
func fetchAllSensors(sensors: inout [any Sensor]) async -> [any Sensor] {
    var allSensors: [any Sensor] = []
    
    var trafficSensors: [TrafficSensor] = []
    var swimmingPoolSensors: [SwimmingPoolSensor] = []
    var plantSensors: [PlantSensor] = []
    var parkingSensors: [ParkingSensor] = []
    var lanternSensors: [LanternSensor] = []
    var co2Sensors: [CO2Sensor] = []
    var waterlevelSensors: [WaterlevelSensor] = []
    
    let sensors = await fetchLocations(sensors: &sensors)
    
    for sensor in sensors {
        switch sensor {
        case let plant as PlantSensor:
            plantSensors.append(plant)
        case let traffic as TrafficSensor:
            trafficSensors.append(traffic)
        case let swimming as SwimmingPoolSensor:
            swimmingPoolSensors.append(swimming)
        case let parking as ParkingSensor:
            parkingSensors.append(parking)
        case let lantern as LanternSensor:
            lanternSensors.append(lantern)
        case let co2 as CO2Sensor:
            co2Sensors.append(co2)
        case let waterlevel as WaterlevelSensor:
            waterlevelSensors.append(waterlevel)
        default:
            allSensors.append(sensor)
        }
    }
    
    //fetch parking
    parkingSensors = await fetchParkingSensors(sensors: &parkingSensors)
    allSensors.append(contentsOf: parkingSensors)
    cacheParking(parking: parkingSensors)
    
    //fetch plants
    plantSensors = await fetchPlantSensors(sensors: &plantSensors)
    allSensors.append(contentsOf: plantSensors)
    cachePlants(plants: plantSensors)
    
    //fetch traffic
    trafficSensors = await fetchTrafficSensors(sensors: &trafficSensors)
    allSensors.append(contentsOf: trafficSensors)
    cacheTraffic(traffic: trafficSensors)
    
    //fetch swimming pools
    swimmingPoolSensors = await fetchSwimmingPoolSensors(sensors: &swimmingPoolSensors)
    allSensors.append(contentsOf: swimmingPoolSensors)
    cacheSwimming(swimming: swimmingPoolSensors)
    
    //fetch co2
    co2Sensors = await fetchCO2Sensors(sensors: &co2Sensors)
    allSensors.append(contentsOf: co2Sensors)
    cacheCO2(co2s: co2Sensors)
    
    //fetch waterlevls
    waterlevelSensors = await fetchWaterlevels(sensors: &waterlevelSensors)
    allSensors.append(contentsOf: waterlevelSensors)
    cacheWaterlevels(levels: waterlevelSensors)
    
    //fetch lanterns
    lanternSensors = await fetchLanterns(sensors: &lanternSensors)
    allSensors.append(contentsOf: lanternSensors)
    cacheLantern(lantern: lanternSensors)
    
    return allSensors
}

func fetchCO2Sensors(sensors: inout [CO2Sensor]) async -> [CO2Sensor] {
    for i in sensors.indices {

        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(CO2Response.self).value
            
            if let observation = val.observations.first {
                let result = observation.result
                
                let data = CO2Data(ppm: result.values.ppm, classification: result.classify.classification.co2ClassificationFromString())
                
                sensors[i].data = data
            }
        } catch {
            logger.error("FrostApi: failed to fetch co2 sensor. error: \(error)")
        }
    }
    
    return sensors
}

func fetchWaterlevels(sensors: inout [WaterlevelSensor]) async -> [WaterlevelSensor] {
    for i in sensors.indices {

        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(WaterlevelResponse.self).value
            
            if let observation = val.observations.first {
                let result = observation.result
                
                let data = WaterlevelData(level: result.level)
                
                sensors[i].data = data
            }
        } catch {
            logger.error("FrostApi: failed to fetch waterlevel sensor. error: \(error)")
        }
    }
    
    return sensors
}

func fetchParkingSensors(sensors: inout [ParkingSensor]) async -> [ParkingSensor] {
    for i in sensors.indices {

        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(ParkingResponse.self).value
            
            if let observation1: ParkingObservation = val.value.first?.observations.first, let observation2: ParkingObservation = val.value.last?.observations.last {
                let result1 = observation1.result
                let result2 = observation2.result
                
                let parkingSlotIsFree1 = result1.state == "free" ? true : false
                let parkingSlotIsFree2 = result2.state == "free" ? true : false
                
                sensors[i].parkingData = ParkingData(free1: parkingSlotIsFree1, free2: parkingSlotIsFree2)
            }
        } catch {
            logger.error("FrostApi: failed to fetch parking sensor. error: \(error)")
        }
    }
    
    return sensors
}

func fetchPlantSensors(sensors: inout [PlantSensor]) async -> [PlantSensor] {
    for i in sensors.indices {

        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(TreeResponse.self).value
            
            if let observation = val.observations.first {
                let result = observation.result
                
                let plantData = PlantData(moisture1: result.moisture1.moistureFromString(), moisture2: result.moisture2.moistureFromString(), moisture3: result.moisture3.moistureFromString())
                sensors[i].plantData = plantData
            }
        } catch {
            logger.error("FrostApi: failed to fetch plant sensor. error: \(error)")
        }
    }
    
    return sensors
}

func fetchTrafficSensors(sensors: inout [TrafficSensor]) async -> [TrafficSensor] {
    for i in sensors.indices {
        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(TrafficResponse.self).value
            
            if let observation = val.observations.first {
                let data = observation.result
                let trafficData = TrafficData(hugeSum: Int(data.hugeSum) ?? 0, largeSum: Int(data.largeSum) ?? 0, mediumSum: Int(data.mediumSum) ?? 0, smallSum: Int(data.smallSum) ?? 0)
                
                sensors[i].trafficData = trafficData
            }
        } catch {
            logger.error("FrostApi: failed to fetch traffic sensor. error: \(error)")
        }
    }
     
    return sensors
}

func fetchSwimmingPoolSensors(sensors: inout [SwimmingPoolSensor]) async -> [SwimmingPoolSensor] {
    for i in sensors.indices {
        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(SwimmingPoolResponse.self).value
            
            if let observation = val.observations.first {
                let res = observation.result
                let data = SwimmingPoolData(
                    sauna: Int(res.values.sauna) ?? 0,
                    outdoor: Int(res.values.outdoor) ?? 0,
                    indoor: Int(res.values.indoor) ?? 0,
                    sauna_capacity: res.capacity.sauna.capacityClassification(),
                    outdoor_capacity: res.capacity.outdoor.capacityClassification(),
                    indoor_capacity: res.capacity.indoor.capacityClassification()
                )
                sensors[i].swimmingPoolData = data
            }
        } catch {
            logger.error("FrostApi: failed to fetch swimming pool sensor. error: \(error)")
        }
    }
    
    return sensors
}

func fetchLanterns(sensors: inout [LanternSensor]) async -> [LanternSensor] {
    for i in sensors.indices {
        do {
            let val = try await AF.request(sensors[i].apiLink).serializingDecodable(LanternResponse.self).value
            
            if let observation = val.observations.first {
                let data = observation.result
                let lanternData = LanternData(brightness: Int(data.brightness) ?? 0)
                
                sensors[i].data = lanternData
            }
        } catch {
            logger.error("FrostApi: failed fetching lanterns. error: \(error)")
        }
    }
    
    return sensors
}


func fetchLocations(sensors: inout [any Sensor]) async -> [any Sensor] {
    for i in sensors.indices {
        if sensors[i].locationLink == "" {
            continue
        }
        
        do {
            let resp = try await AF.request(sensors[i].locationLink).serializingDecodable(LocationResponse.self).value
            
            if let val = resp.val.first {
                let lat = val.location.coordinates[1]
                let lng = val.location.coordinates[0]
                sensors[i].lat = lat
                sensors[i].lng = lng
            }
            
        } catch {
            logger.error("FrostApi: \(error)")
            let sensor = sensors[i]
            logger.error("FrostApi: could not fetch location for: \(sensor.id):\(sensor.name) ")
        }
    }
    
    return sensors
}
