//
//  LanternResponse.swift
//  Aurea
//
//  Created by lichen on 14.06.23.
//

import Foundation

struct LanternResponse: Codable {
    let observations: [LanternObservation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct LanternObservation: Codable {
    let result: LanternObservationResult
}

struct LanternObservationResult: Codable {
    let brightness: String
    
    private enum CodingKeys: String, CodingKey {
        case brightness = "led_brightness"
    }
}
