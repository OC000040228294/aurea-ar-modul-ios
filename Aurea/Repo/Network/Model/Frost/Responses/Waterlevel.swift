//
//  Waterlevel.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import Foundation

struct WaterlevelResponse: Codable {
    let observations: [WaterlevelObservation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct WaterlevelObservation: Codable {
    let phenomenonTime: String
    let resultTime: String
    let result: WaterlevelObservationResult
}

struct WaterlevelObservationResult: Codable {
    let level: String
    
    private enum CodingKeys: String, CodingKey {
        case level = "stand"
    }
}
