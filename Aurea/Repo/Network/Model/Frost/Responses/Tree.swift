//
//  Tree.swift
//  CityPad
//
//  Created by lichen on 08.12.22.
//

import Foundation

struct TreeResponse: Codable {
    let observations: [TreeObservation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct TreeObservation: Codable {
    let phenomenonTime: String
    let resultTime: String
    let result: TreeObservationResult
}

struct TreeObservationResult: Codable {
    let moisture1: String
    let moisture2: String
    let moisture3: String
    
    private enum CodingKeys: String, CodingKey {
        case moisture1 = "soil_moisture_1"
        case moisture2 = "soil_moisture_2"
        case moisture3 = "soil_moisture_3"
    }
}

extension String {
    func moistureFromString() -> PlantMoisture {
        switch self {
        case "low":
            return .low
        case "medium":
            return .medium
        default:
            return .high
        }
    }
    
    func co2ClassificationFromString() -> CO2Classification {
        switch self {
        case "Normal":
            return .normal
        case "Noticeable":
            return .noticeable
        default: return .unacceptable
        }
    }
}
