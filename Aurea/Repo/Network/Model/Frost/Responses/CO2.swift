//
//  CO2.swift
//  CityPad
//
//  Created by lichen on 02.05.23.
//

import Foundation

struct CO2Response: Codable {
    let observations: [CO2Observation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct CO2Observation: Codable {
    let result: CO2ObservationResult
}

struct CO2ObservationResult: Codable {
    let values: CO2ObservationResultValues
    let classify: CO2ObservationResultClassify
}

struct CO2ObservationResultValues: Codable {
    let ppm: String
}

struct CO2ObservationResultClassify: Codable {
    let classification: String
    
    private enum CodingKeys: String, CodingKey {
        case classification = "class"
    }
}
