//
//  TrafficResponse.swift
//  CityPad
//
//  Created by lichen on 27.09.22.
//

import Foundation

struct TrafficResponse: Codable {
    let observations: [TrafficObservation]

    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct TrafficObservation: Codable {
    let phenomenonTime: String
    let resultTime: String
    let result: TrafficObservationResultData
}

struct TrafficObservationResultData: Codable {
    let hugeSum: String
    let largeSum: String
    let mediumSum: String
    let smallSum: String
    
    private enum CodingKeys: String, CodingKey {
        case hugeSum = "huge_sum"
        case largeSum = "large_sum"
        case mediumSum = "medium_sum"
        case smallSum = "small_sum"
    }
}
