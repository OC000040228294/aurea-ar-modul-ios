//
//  Parking.swift
//  CityPad
//
//  Created by lichen on 01.11.22.
//

import Foundation

struct ParkingResponse: Codable {
    let value: [ParkingValue]
}

struct ParkingValue: Codable {
    let observations: [ParkingObservation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "Observations"
    }
}

struct ParkingObservation: Codable {
    let result: ParkingObservationResult
}

struct ParkingObservationResult: Codable {
    let state: String
    
    private enum CodingKeys: String, CodingKey {
        case state = "p_state"
    }
}
