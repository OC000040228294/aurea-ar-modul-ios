//
//  SwimmingPool.swift
//  CityPad
//
//  Created by lichen on 10.10.22.
//

import Foundation

struct SwimmingPoolResponse: Codable {
    let observations: [SwimmingPoolObservation]
    
    private enum CodingKeys: String, CodingKey {
        case observations = "value"
    }
}

struct SwimmingPoolObservation: Codable {
    let result: SwimmingPoolObservationResult
}

struct SwimmingPoolObservationResult: Codable {
    let values: SwimmingPoolObservationValues
    let capacity: SwimmingPoolObservationValues
}

struct SwimmingPoolObservationValues: Codable {
    let sauna: String
    let indoor: String
    let outdoor: String
    
    private enum CodingKeys: String, CodingKey {
        case sauna = "sauna", indoor = "hallenbad", outdoor = "freibad"
    }
}

extension String {
    
    func capacityClassification() -> PoolCapacity {
        switch self {
        case "Low":
            return .LOW
        case "Medium":
            return .MEDIUM
        case "High":
            return .HIGH
        default: return .UNKNOWN
        }
    }
}
