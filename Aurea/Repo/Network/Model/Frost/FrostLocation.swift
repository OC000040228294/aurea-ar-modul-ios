//
//  FrostLocation.swift
//  CityPad
//
//  Created by lichen on 30.01.23.
//

import Foundation

struct LocationResponse: Codable {
    let val: [LocationValue]
    
    private enum CodingKeys: String, CodingKey {
        case val = "value"
    }
}

struct LocationValue: Codable {
    let location: FrostLocation
}

struct FrostLocation: Codable {
    let coordinates: [Double]
}
