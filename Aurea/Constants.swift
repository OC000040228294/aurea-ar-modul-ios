//
//  Constants.swift
//  Aurea
//
//  Created by Sebastian Lange on 20.10.23.
//

import Foundation
import OSLog

let logger = Logger()

public let API_DATA_REFRESH_INTERVAL: UInt64 = 15_000_000_000 //every 15 seconds refresh
