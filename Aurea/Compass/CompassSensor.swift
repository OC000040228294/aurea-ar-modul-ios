//
//  CompassSensor.swift
//  CityPad
//
//  Created by lichen on 05.10.22.
//

import Foundation
import CoreLocation
import DequeModule
import CityPadFramework

/// Model for the compass sensor
/// It is used to display the compass sensors in the compass overlay view
/// @param id: The id of the sensor
/// @param name: The name of the sensor
/// @param distance: The distance to the sensor
/// @param sensors: The sensors that are clustered into this sensor
/// @param location: The location of the sensor
/// @param angle: The angle pointing to the sensor
struct CompassSensor: Identifiable, Equatable {
    var id: String
    var name: String
    var distance: Double
    var sensors: [any Sensor]
    var location: CLLocation
    var angle: Double
    
    var isCluster: Bool {
        return sensors.count > 1
    }
    
    static func ==(lhs: CompassSensor, rhs: CompassSensor) -> Bool {        
        return lhs.id == rhs.id
    }
    
    /// Clusters the given sensor with the current sensor
    mutating func cluster(other: CompassSensor) {
        sensors.append(contentsOf: other.sensors)
        sensors.sort {
            $0.distance < $1.distance
        }
        id = sensors[0].id
        distance = sensors[0].distance
        name = sensors[0].name
    }
    
    /// Returns the asset raw string matching the type of the sensor
    var type: String {
        if let first = sensors.first {
            return first.toTypeImage()
        } else {
            return ImageString.Closed.rawValue
        }
    }

    /// Clusters all given sensors into one
    static func clusterAllCompassSensorsIntoOne(sensors: [CompassSensor]) -> CompassSensor? {
        var stack = Deque(sensors)
      
        if var mainSensor = stack.popFirst() {
            while !stack.isEmpty {
                let next = stack.removeFirst()
                mainSensor.cluster(other: next)
            }
            
            return mainSensor
        }
        
        return nil
    }
    
    static let exampleData = CompassSensor(id: "0", name: "Test 1", distance: 500.0, sensors: sensorExampleData, location: aureaPosition(), angle: 0)
    
    /// Creates a CompassItem from the current CompassSensor
    func toCompassItem() -> CompassItem {
        let libSensors = sensors.map { sensor in
            sensor.toLibSensor()
        }
        return CompassItem(id: self.id, name: name, distance: self.distance, sensors: libSensors, location: self.location, angle: self.angle)
    }
}

extension CompassSensor {
    func compassAngle(rotation: Double) -> Double {
        var angle = abs(rotation - self.angle)
        if angle > 360.0 {
            angle = angle.remainder(dividingBy: 360.0)
        }
        
        if angle > 180.0 {
            angle = angle - 360.0
            angle = -1 * angle
        }
        
        return angle
    }
}

extension Sensor {
    func toTypeImage() -> String {
        switch self.type {
        case .PLANT:
            return ImageString.PlantSensor.rawValue
        case .TRAFFIC:
            return ImageString.TrafficSensor.rawValue
        case .SWIMMING:
            return ImageString.SwimmingPool.rawValue
        case .PARKING:
            return ImageString.Parking.rawValue
        case .LANTERN:
            return ImageString.Lantern.rawValue
        case .CO2:
            return ImageString.CO2.rawValue
        case .WATERLEVEL:
            return ImageString.Waterlevel.rawValue
        default:
            return ImageString.Placed.rawValue
        }
    }
}
