//
//  CompassOverlayView.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI
import CityPadFramework

/// View for the compass overlay
/// It is used to display the compass overlay with the distance rings
struct CompassOverlayView: View {
    @EnvironmentObject private var model: ModelData
    @StateObject private var locationModel = LocationModel.shared
    @EnvironmentObject var mainState: MainState
    @EnvironmentObject private var state: CompassState
    
    func coneRadius(status: RingStatus) -> Double {
        switch status {
        case .nearby:
            return 0.0
        case .inner:
            return compassRingRadius1 + 0.07
        case .middle:
            return compassRingRadius2 + 0.12
        case.outer:
            return compassRingRadius3 + 0.17
        }
    }
    
    var body: some View {
        let rotation = locationModel.heading
        
        GeometryReader { geometry in
            let w: Double = geometry.size.width
            let h: Double = geometry.size.height
            let wh: Double = w < h ? w : h
            
            let centerX: Int = Int(wh / 2)
            let centerY: Int = Int(wh / 2)
            let radarW: Double = wh
            let radarRadius: Double = radarW / 2
            
            VStack {
                ZStack {
                    // compass background
                    CompassView(rotation: rotation)
                    
                    // raycast cones that indicate the angle range to lock into a sensor and the range to release the lock
                    Cone(degree: Int(RAYCAST_ANGLE_LOCK_THRESHOLD) * 2, radarRadiusInPercent: 0.9)
                        .fill(LinearGradient(colors: [Color(.smartBlue), .white], startPoint: .top, endPoint: .bottom))
                        .opacity(0.3)
                        .frame(width: wh, height: wh * coneRadius(status: state.currentRing))
                        .clipShape(
                            Cone(degree: Int(RAYCAST_ANGLE_THRESHOLD) * 2, radarRadiusInPercent: 0.9)
                        )
                    
                    Cone(degree: Int(RAYCAST_ANGLE_THRESHOLD) * 2, radarRadiusInPercent: 0.9)
                        .fill(LinearGradient(colors: [Color(.smartBlue), .white], startPoint: .top, endPoint: .bottom))
                        .opacity(0.15)
                        .frame(width: wh, height: wh * coneRadius(status: state.currentRing))
                    
                    // compass rings
                    CompassCurrentRingView(currentRing: state.currentRing, width: wh)
                    
                    CompassRing(sensors: state.ring1, selectedSensor: state.selectedSensor, centerX: centerX, centerY: centerY, rotation: rotation, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius1,
                                isFocused: state.currentRing == .inner)
                    
                    CompassRing(sensors: state.ring2, selectedSensor: state.selectedSensor, centerX: centerX, centerY: centerY, rotation: rotation, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius2,
                                isFocused: state.currentRing == .middle)
                    
                    CompassRing(sensors: state.ring3, selectedSensor: state.selectedSensor, centerX: centerX, centerY: centerY, rotation: rotation, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius3,
                                isFocused: state.currentRing == .outer)
                    
                    // compass sensor icons that are nearby
                    if let nearby = state.nearby {
                        let focus = state.currentRing == .nearby ? 0.5 : 0.2
                        let opacity = state.selectedSensor == nearby ? 1.0 : focus
                        if nearby.isCluster {
                            ClusterIconView(count: nearby.sensors.count, isFocused: state.currentRing == .nearby)
                                .position(x: CGFloat(centerX), y: CGFloat(centerY))
                                .opacity(opacity)
                        } else {
                            CompassSensorView(type: nearby.type, isFocused: state.currentRing == .nearby)
                                .position(x: CGFloat(centerX), y: CGFloat(centerY))
                                .opacity(opacity)
                        }
                    }
                    
                    // nearby lottie animation
                    if state.nearby != nil && state.currentRing == .nearby {
                        LottieView(filename: "nearby", repeating: .infinity)
                            .clipShape(Circle())
                            .frame(width: 60)
                        
                    }
                }
                .frame(width: wh, height: wh)
                
                Spacer()
            }
            
        }
        .padding(30)
    }
}

struct CompassOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        CompassOverlayView()
            .environmentObject(ModelData.shared)
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
            .environmentObject(MainState.shared)
    }
}
