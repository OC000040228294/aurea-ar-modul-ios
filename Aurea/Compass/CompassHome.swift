//
//  CompassHome.swift
//  CityPad
//
//  Created by lichen on 16.05.22.
//

import SwiftUI

let artefactSize = 30.0

/// Main View for the compass feature
/// It is used to display the compass view with the compass overlay view and the compass screen extras
struct CompassHome: View {
    @EnvironmentObject private var state: CompassState
    
    var body: some View {        
        ZStack {
            Color(.background)
            //compass overlay view with sensors
            CompassOverlayView()
                .environmentObject(state)
            
            VStack {
                Spacer()
                
                HStack(alignment: .bottom) {
                    FloatingRingMenu(
                        ring: $state.currentRing,
                        nearbyEnabled: state.nearby != nil,
                        innerEnabled: !state.ring1.isEmpty,
                        middleEnabled: !state.ring2.isEmpty,
                        outerEnabled: !state.ring3.isEmpty
                    ) {
                        state.setCompassRingState(state: .nearby)
                    } innerClicked: {
                        state.setCompassRingState(state: .inner)
                    } middleClicked: {
                        state.setCompassRingState(state: .middle)
                    } outerClicked: {
                        state.setCompassRingState(state: .outer)
                    }
                    
                    Spacer()
                    
                    //if any sensor is discovered, show discovered sheet
                    if let selected = state.lockedSensor {
                        if state.nearby != nil && state.currentRing == .nearby {
                            CompassNearbyDiscoveredOverlay(
                                name: selected.name,
                                type: selected.type,
                                distance: Int(currentNearbyThresholdMeters))
                        } else {
                            CompassDiscoveredOverlay(
                                name: selected.name,
                                isCluster: selected.isCluster,
                                type: selected.type,
                                distance: Int(selected.distance))
                        }
                    }
                }
                .padding(16)
            }
            
            //extras like buttons to press, logo image
            CompassScreenExtras()
        }
        .environmentObject(state)
        .gesture(dragGesture)
    }
    
    private var dragGesture: some Gesture {
        DragGesture(minimumDistance: 3.0, coordinateSpace: .local)
            .onEnded { value in
                // left swipe
                if value.translation.width < 0 && value.translation.height > -30 && value.translation.height < 30 {
                    state.nextRing()
                }
                // right swipe
                else if value.translation.width > 0 && value.translation.height > -30 && value.translation.height < 30 {
                    state.prevRing()
                }
                // up swipe
                else if value.translation.height < 0 && value.translation.width < 100 && value.translation.width > -100 {
                    state.nextRing()
                }
                // down swipe
                else if value.translation.height > 0 && value.translation.width < 100 && value.translation.width > -100 {
                    state.prevRing()
                }
            }
    }
}

struct CompassHome_Previews: PreviewProvider {
    static var previews: some View {
        CompassHome()
            .environmentObject(ModelData.shared)
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
            .environmentObject(MainState.shared)
        CompassHome()
            .preferredColorScheme(.dark)
            .environmentObject(ModelData.shared)
            .environmentObject(CompassState(model: ModelData.shared, mainState: MainState()))
            .environmentObject(MainState.shared)
            
    }
}
