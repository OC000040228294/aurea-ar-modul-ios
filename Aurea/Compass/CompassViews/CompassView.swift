//
//  CompassView.swift
//  CityPad
//
//  Created by lichen on 13.09.22.
//

import SwiftUI

/// View for the compass
/// It is used to display the compass background in the compass view aspect the given rotation
struct CompassView: View {
    let rotation: Double
    
    var body: some View {
        Image(ImageString.Compass.rawValue)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .scaledToFit()
            .rotationEffect(Angle(degrees: rotation))
    }
}

struct CompassView_Previews: PreviewProvider {
    static var previews: some View {
        CompassView(rotation: 0.0)
            .previewLayout(.sizeThatFits)
        
        CompassView(rotation: 0.0)
            .previewLayout(.sizeThatFits)
            .preferredColorScheme(.dark)
    }
}
