//
//  CompassHomeItemView.swift
//  CityPad
//
//  Created by lichen on 02.08.22.
//

import SwiftUI

struct CompassHomeItemView: View {
    let x: Double
    let y: Double
    
    var body: some View {
        Image(systemName: "location.north.fill")
            .resizable()
            .foregroundColor(Color(.smartBlue))
            .frame(width: 20, height: 25)
            .position(x: x, y: y)
    }
}

struct CompassHomeItemView_Previews: PreviewProvider {
    static var previews: some View {
        CompassHomeItemView(x: 0.0, y: 0.0)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
