//
//  CompassItemView.swift
//  CityPad
//
//  Created by lichen on 02.08.22.
//

import SwiftUI

/// View for the compass items
/// It is used to display the items in the compass view
struct CompassItemView: View {
    let x: Double
    let y: Double
    let itemType: String
    
    var body: some View {
        Image(itemType)
            .resizable()
            .frame(width: artefactSize, height: artefactSize)
            .scaledToFill()
            .position(x: x, y: y)
    }
}

struct CompassItemView_Previews: PreviewProvider {
    static var previews: some View {
        CompassItemView(x: 0.0, y: 0.0, itemType: ImageString.Attraction.rawValue)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
