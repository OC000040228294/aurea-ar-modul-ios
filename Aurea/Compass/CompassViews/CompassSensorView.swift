//
//  CompassSensorView.swift
//  CityPad
//
//  Created by lichen on 05.10.22.
//

import SwiftUI

/// View for the compass sensors
/// It is used to display the sensor icon in the compass view
struct CompassSensorView: View {
    let type: String
    var isFocused: Bool = true
    
    var image: String {
        !isFocused ? "\(type)_grey" : type
    }
    
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFit()
            .frame(width: 30)
    }
}

struct CompassSensorView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Image(.compassv2)
                .resizable()
                .scaledToFit()

            CompassSensorView(type: ImageString.Lantern.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 0, y: 0)
            
            CompassSensorView(type: ImageString.PlantSensor.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: -50, y: -50)
            
            CompassSensorView(type: ImageString.Parking.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 50, y: -50)
            
            CompassSensorView(type: ImageString.TrafficSensor.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: -50, y: 50)
            
            CompassSensorView(type: ImageString.SwimmingPool.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 50, y: 50)
            
            CompassSensorView(type: ImageString.Waterlevel.rawValue)
                .padding()
                .previewLayout(.sizeThatFits)
                .offset(x: 0, y: 50)
        }
        .modifier(TopBoundModifier())
    }
}
