//
//  CompassRing.swift
//  CityPad
//
//  Created by lichen on 08.02.23.
//

import SwiftUI

/// View for the compass ring
/// It is used to display the compass ring with the sensors
/// - Parameters:
///     - sensors: the CompassSensors to display
///     - selectedSensor: the selected sensor
///     - centerX: the x coordinate of the center
///     - centerY: the y coordinate of the center
///     - rotation: the rotation of the compass
///     - radarRadius: the radius of the compass
///     - ringDistancePercentage: the radius of the ring in percent
///     - isFocused: the focus state of the ring
struct CompassRing: View {
    var sensors: [CompassSensor]
    var selectedSensor: CompassSensor?
    let centerX: Int
    let centerY: Int
    let rotation: Double
    let radarRadius: Double
    let ringDistancePercentage: Double
    var isFocused: Bool = false
    
    var opacity: Double {
        if isFocused {
            return 0.5
        } else {
            return 0.2
        }
    }
    
    var body: some View {
        ForEach(sensors) {sensor in
            let (offX, offY) = calcOffset(centerX, centerY, sensor.angle, rotation, radarRadius, ringDistancePercentage)
            if let selected = selectedSensor {
                let opacity = sensor.id == selected.id ? 1.0 : opacity
                let posX = sensor.id == selected.id && sensor.distance < currentNearbyThresholdMeters ? Double(centerX) : offX
                let posY = sensor.id == selected.id && sensor.distance < currentNearbyThresholdMeters ? Double(centerY) - 20.0 : offY
                
                if sensor.isCluster {
                    ClusterIconView(count: sensor.sensors.count, isFocused: isFocused)
                        .position(x: posX, y: posY)
                        .opacity(opacity)
                } else {
                    CompassSensorView(type: sensor.type, isFocused: isFocused)
                        .position(x: posX, y: posY)
                        .opacity(opacity)
                }
            } else {
                if sensor.isCluster {
                    ClusterIconView(count: sensor.sensors.count, isFocused: isFocused)
                        .position(x: offX, y: offY)
                        .opacity(opacity)
                } else {
                    CompassSensorView(type: sensor.type, isFocused: isFocused)
                        .position(x: offX, y: offY)
                        .opacity(opacity)
                }
            }
        }
    }
}


func calcOffset(_ x: Int, _ y: Int, _ angle: Double,_ rotation: Double, _ radarRadius: Double, _ ringDistancePercentage: Double) -> (Double, Double) {
    let artefactRadius = radarRadius * ringDistancePercentage
    let deg = angle + rotation
    
    let radians = deg.toRadians()
    
    let newX = Double(x) + sin(radians) * artefactRadius
    let newY = Double(y) - cos(radians) * artefactRadius
    
    return (newX, newY)
}

struct CompassRing_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {geometry in
            let w: Double = geometry.size.width
            let h: Double = geometry.size.height
            let wh: Double = w < h ? w : h
            
            let centerX: Int = Int(wh / 2)
            let centerY: Int = Int(wh / 2)
            let radarW: Double = wh
            let radarRadius: Double = radarW / 2
            
            VStack {
                ZStack {
                    CompassView(rotation: 0)
                    CompassCurrentRingView(currentRing: .inner, width: wh)
                    CompassRing(sensors: [CompassSensor.exampleData], centerX: centerX, centerY: centerY, rotation: 0, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius1)
                }
                .frame(width: wh, height: wh)
            }
        }
        
        GeometryReader {geometry in
            let w: Double = geometry.size.width
            let h: Double = geometry.size.height
            let wh: Double = w < h ? w : h
            
            let centerX: Int = Int(wh / 2)
            let centerY: Int = Int(wh / 2)
            let radarW: Double = wh
            let radarRadius: Double = radarW / 2
            
            VStack {
                ZStack {
                    CompassView(rotation: 0)
                    CompassCurrentRingView(currentRing: .middle, width: wh)
                    CompassRing(sensors: [CompassSensor.exampleData], centerX: centerX, centerY: centerY, rotation: 0, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius2)
                }
                .frame(width: wh, height: wh)
            }
        }
        
        GeometryReader {geometry in
            let w: Double = geometry.size.width
            let h: Double = geometry.size.height
            let wh: Double = w < h ? w : h
            
            let centerX: Int = Int(wh / 2)
            let centerY: Int = Int(wh / 2)
            let radarW: Double = wh
            let radarRadius: Double = radarW / 2
            
            VStack {
                ZStack {
                    CompassView(rotation: 0)
                    CompassCurrentRingView(currentRing: .outer, width: wh)
                    CompassRing(sensors: [CompassSensor.exampleData], centerX: centerX, centerY: centerY, rotation: 0, radarRadius: radarRadius, ringDistancePercentage: compassRingRadius3)
                }
                .frame(width: wh, height: wh)
            }
        }
    }
}
