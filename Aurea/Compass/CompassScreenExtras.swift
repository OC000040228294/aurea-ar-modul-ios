//
//  CompassScreenExtras.swift
//  CityPad
//
//  Created by lichen on 03.02.23.
//

import SwiftUI
import AVKit

/// View for the compass screen extras
/// It is used to display the compass screen extras like the logo
struct CompassScreenExtras: View {
    @EnvironmentObject var mainState: MainState
    @EnvironmentObject private var state: CompassState
    
    @AppStorage("useSounds") var useSounds = true
    @AppStorage("useVibrations") var useVibrations = true
    @State var audioPlayer: AVAudioPlayer!
    
    let haptics = UIImpactFeedbackGenerator(style: .medium)
    let haptics2 = UIImpactFeedbackGenerator(style: .rigid)
    let haptics3 = UIImpactFeedbackGenerator(style: .heavy)
    let haptics4 = UINotificationFeedbackGenerator()
    
    var body: some View {
        VStack {//place item overlay
            HStack {
                
                #warning("remove later")
                //DevButton(mainState: mainState)
                Spacer()
                
                Image(.logoSmartkassel)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 60)
                    .padding(4)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color.white).shadow(radius: 2))
                    .padding(.top)
                    .padding(.trailing)
            }
            
            Spacer()
        }
        .onReceive(state.hapticSubject) {haptic in
            if useVibrations {
                if haptic == .Selected {
                    haptics2.impactOccurred()
                } else if haptic == .Unselected {
                    haptics.impactOccurred()
                }
            }
        }
        .onReceive(state.lockedSubject) {locked in
            if locked {
                if useSounds {
                    playSounds("locked.mp3")
                }
                if useVibrations {
                    haptics3.impactOccurred()                    
                }
            }
        }
        .onReceive(state.ringLimitReachedSubject) {limit in
            if limit && useVibrations {
                haptics4.notificationOccurred(.warning)
            }
        }
    }
    
    func playSounds(_ soundFileName : String) {
        guard let soundURL = Bundle.main.url(forResource: soundFileName, withExtension: nil) else {
            fatalError("Unable to find \(soundFileName) in bundle")
        }

        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
        } catch {
            print(error.localizedDescription)
        }
        audioPlayer.play()
    }
}

struct CompassScreenExtras_Previews: PreviewProvider {
    static var previews: some View {
        CompassScreenExtras()
            .environmentObject(CompassState(model: ModelData(), mainState: MainState.shared))
            .environmentObject(MainState.shared)
    }
}
