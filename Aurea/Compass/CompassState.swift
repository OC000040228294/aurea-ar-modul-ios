//
//  CompassState.swift
//  CityPad
//
//  Created by lichen on 01.06.22.
//

import Foundation
import Combine
import CoreLocation
import CityPadFramework

let NEARBY_THRESHHOLD_LOW = 3.5
let NEARBY_THRESHHOLD_HIGH = 5.5
var currentNearbyThresholdMeters = NEARBY_THRESHHOLD_LOW

let RANDOM_ITEM_MAX_DISTANCE = 100.0
let NEARBY_RING_RADIUS = 0.1

// Distance Threshold for Compass Ring 1 in meters
let compassRing1 = 50.0
// Radius of Compass Ring 1 in percent
let compassRingRadius1 = 0.23
// Distance Threshold for Compass Ring 2 in meters
let compassRing2 = 200.0
// Radius of Compass Ring 2 in percent
let compassRingRadius2 = 0.44
// Radius of Compass Ring 3 in percent
let compassRingRadius3 = 0.65

enum RingStatus {
    case nearby, inner, middle, outer
}

/// State for the compass feature
///
/// It is used to calculate the compass state
@MainActor
final class CompassState: ObservableObject {
    @Published var selectedSensor: CompassSensor? = nil
    var lockableSensor: CompassSensor? = nil
    @Published var lockedSensor: CompassSensor? = nil
    
    @Published var nearby: CompassSensor? = nil
    @Published var ring1: [CompassSensor] = []
    @Published var ring2: [CompassSensor] = []
    @Published var ring3: [CompassSensor] = []
    @Published var currentRing: RingStatus = .nearby
    
    let mainState: MainState
    let model: ModelData
    private var locationModel = LocationModel.shared
    private var tabState = TabStateManager.shared
    private var headingSubscription: AnyCancellable?
    private var locationSubscription: AnyCancellable?
    private var sensorsSubscription: AnyCancellable?
    
    let hapticSubject = PassthroughSubject<CityPadUtils.Haptics, Never>()
    let lockedSubject = PassthroughSubject<Bool, Never>()
    let ringLimitReachedSubject = PassthroughSubject<Bool, Never>()
    
    /// Select the next compass ring outwards
    func nextRing() {
        let ringBefore = currentRing
        
        switch currentRing {
        case .nearby:
            if !self.ring1.isEmpty {
                currentRing = .inner
            } else if !self.ring2.isEmpty {
                currentRing = .middle
            } else if !self.ring3.isEmpty {
                currentRing = .outer
            }
        case .inner:
            if !self.ring2.isEmpty {
                currentRing = .middle
            } else if !self.ring3.isEmpty {
                currentRing = .outer
            }
        case .middle:
            if !self.ring3.isEmpty {
                currentRing = .outer
            }
        case .outer:
            currentRing = .outer
        }
        
        if ringBefore == currentRing {
            self.ringLimitReachedSubject.send(true)
        }
        
        self.calculateState()
    }
    
    func ringLimitReachedVibration() {
        self.ringLimitReachedSubject.send(true)
    }
    
    /// Select the next compass ring inwards
    func prevRing() {
        let ringBefore = currentRing
        
        switch currentRing {
        case .nearby:
            currentRing = .nearby
        case .inner:
            if self.nearby != nil {
                currentRing = .nearby
            }
        case .middle:
            if !self.ring1.isEmpty {
                currentRing = .inner
            } else if self.nearby != nil {
                currentRing = .nearby
            }
        case .outer:
            if !self.ring2.isEmpty {
                currentRing = .middle
            } else if !self.ring1.isEmpty {
                currentRing = .inner
            } else if self.nearby != nil {
                currentRing = .nearby
            }
        }
        
        if ringBefore == currentRing {
            self.ringLimitReachedSubject.send(true)
        }
        
        self.calculateState()
    }
    
    /// Init the compass state
    init(model: ModelData, mainState: MainState) {
        self.model = model
        self.mainState = mainState
        
        sensorsSubscription = model.$sensors.sink{[weak self] _ in
            DispatchQueue.main.async {
                self?.calculateState()
            }
        }
        
        headingSubscription = locationModel.$heading.sink{[weak self] _ in
            self?.calculateState()
        }
        
        locationSubscription = locationModel.$location.sink{[weak self] _ in
            self?.calculateState()
        }
    }
    
    private var lockTask: Task<Void, Never>?
    
    private func lockSensorAfterDelay() {
        lockTask?.cancel()
        
        lockTask = Task {
            do {
                try await Task.sleep(nanoseconds: 300_000_000)
            } catch {
            }
            guard !Task.isCancelled else { return }
            lockSensor()
        }
        //lockSensor()
    }

    private func cancelLocking() {
        lockTask?.cancel()
    }
    
    private func lockSensor() {
        DispatchQueue.main.async {
            if self.selectedSensor != nil {
                self.lockedSensor = self.selectedSensor
                
                self.mainState.targetSensors(sensors: self.selectedSensor!.sensors)
                
                self.lockedSubject.send(true)
            }
        }
    }
    
    func setCompassRingState(state: RingStatus) {
        if state == .nearby && self.nearby != nil {
            currentRing = .nearby
        } else if state == .inner && !self.ring1.isEmpty {
            currentRing = .inner
        } else if state == .middle && !self.ring2.isEmpty {
            currentRing = .middle
        } else if state == .outer && !self.ring3.isEmpty {
            currentRing = .outer
        }
        
        self.calculateState()
    }

    func calculateState() {
        if MotionManager.shared.swapToAr {
            return
        }
        
        if CityPadUtils.noLocationPermission(status: locationModel.authorizationStatus) {
            return
        }
        
        if tabState.state != .main {
            return
        }
        
        let location = mainState.devModeSimulateLocation == true ? aureaPosition() :  locationModel.location
        let rotation = -locationModel.heading
        let sensors = model.sensors
        
        let libSensors = sensors.map {  sensor in
            sensor.toLibSensor()
        }
        
        let compassSensors: [CompassSensor] = CompassCalculations.calculateCompassItems(
            from: libSensors,
            position: location,
            rotation: rotation
        ).map { item in
            getCompassSensorFromCompassItem(compassItem: item, sensors: sensors)
        }
        
        var nearby: [CompassSensor] = []
        var ring1: [CompassSensor] = []
        var ring2: [CompassSensor] = []
        var ring3: [CompassSensor] = []
    
        var noNearby = true
        
        compassSensors.forEach {sensor in
            if sensor.distance < currentNearbyThresholdMeters {
                noNearby = false
                nearby.append(sensor)
            } else if sensor.distance < compassRing1 {
                ring1.append(sensor)
            } else if sensor.distance < compassRing2 {
                ring2.append(sensor)
            } else {
                ring3.append(sensor)
            }
        }
        
        if noNearby {
            currentNearbyThresholdMeters = NEARBY_THRESHHOLD_LOW
        } else {
            currentNearbyThresholdMeters = NEARBY_THRESHHOLD_HIGH
        }
        
        self.nearby = CompassSensor.clusterAllCompassSensorsIntoOne(sensors: nearby)
        // Cluster items for ring 1 by anglethreshold
        self.ring1 = CompassCalculations.clusterCompassItems(
            items: ring1.map { $0.toCompassItem() },
            angle: RAYCAST_ANGLE_THRESHOLD,
            nearbyThreshold: currentNearbyThresholdMeters
        )
        .map { item in
            getCompassSensorFromCompassItem(compassItem: item, sensors: sensors)
        }
        // Cluster items for ring 2 by anglethreshold
        self.ring2 = CompassCalculations.clusterCompassItems(
            items: ring2.map { $0.toCompassItem() },
            angle: RAYCAST_ANGLE_THRESHOLD,
            nearbyThreshold: currentNearbyThresholdMeters
        )
        .map { item in
            getCompassSensorFromCompassItem(compassItem: item, sensors: sensors)
        }
        // Cluster items for ring 3 by anglethreshold
        self.ring3 = CompassCalculations.clusterCompassItems(
            items: ring3.map { $0.toCompassItem() },
            angle: RAYCAST_ANGLE_THRESHOLD,
            nearbyThreshold: currentNearbyThresholdMeters
        )
        .map { item in
            getCompassSensorFromCompassItem(compassItem: item, sensors: sensors)
        }
        
        setCurrentRing()
        
        if let nearby = self.nearby {
            if currentRing == .nearby {
                // Set the fixed location to the current location
                mainState.fixedLocation = location
                // Calculate the nearby location for nearby ar mode
                mainState.nearbyLocation = CityPadUtils.manufactureNearbyLocation(location: location, heading: rotation)
                mainState.useFixedNearbyLocation = true
                
                mainState.targetSensors(sensors: nearby.sensors)
                sensorNewSelection(nearby, self.selectedSensor)
                
                self.selectedSensor = nearby
                self.lockedSensor = nearby
            } else {
                checkSelectedSensor(rotation: rotation)
                mainState.useFixedNearbyLocation = false
                
            }
        } else {
            checkSelectedSensor(rotation: rotation)
        }
    }
    
    func getCompassSensorFromCompassItem(compassItem: CompassItem, sensors: [any Sensor]) -> CompassSensor  {
            let tmpSensors = compassItem.sensors.map{ libSensor in
                var sensor = sensors.first(where: { $0.id == libSensor.id })!
                sensor.distance = libSensor.distance
                return sensor
            }
            return CompassSensor(
                id: compassItem.id,
                name: compassItem.name,
                distance: compassItem.distance,
                sensors: tmpSensors,
                location: compassItem.location,
                angle: compassItem.angle)
    }
    
    func setCurrentRing() {
        var switchRing = false
        
        if currentRing == .nearby && self.nearby == nil {
            switchRing = true
        } else if currentRing == .inner && self.ring1.isEmpty {
            switchRing = true
        } else if currentRing == .middle && self.ring2.isEmpty {
            switchRing = true
        } else if currentRing == .outer && self.ring3.isEmpty {
            switchRing = true
        }
        
        if switchRing && self.nearby != nil {
            currentRing = .nearby
        } else if switchRing && !ring1.isEmpty {
            currentRing = .inner
        } else if switchRing && !ring2.isEmpty {
            currentRing = .middle
        } else if switchRing && !ring3.isEmpty {
            currentRing = .outer
        }
    }
    
    func currentRingSensors() -> [CompassSensor] {
        switch currentRing {
        case .inner:
            return ring1
        case .middle:
            return ring2
        case .outer:
            return ring3
        default:
            return ring1
        }
    }
    
    func checkSelectedSensor(rotation: Double) {
        let ring = currentRingSensors()
        
        let selectedItem = CompassCalculations.checkCompassItemSelection(
            compassItems: ring.map { $0.toCompassItem() },
            rotation: rotation)
        let lockableItem = CompassCalculations.checkCompassItemSelectionLock(
            items: ring.map { $0.toCompassItem() },
            rotation: rotation)

        let selected = selectedItem == nil ? nil : ring.first(where: { $0.id == selectedItem!.id })
        let lockable = lockableItem == nil ? nil : ring.first(where: { $0.id == lockableItem!.id })
        
        sensorNewSelection(selected, self.selectedSensor)
        sensorNewLock(lockable, self.lockableSensor)
        
        self.lockableSensor = lockable
        self.selectedSensor = selected
        
        
        
        if self.lockedSensor != nil {
            self.lockedSensor = lockable
        }
        
        DispatchQueue.main.async {
            if selected == nil || (selected != self.lockedSensor && self.lockedSensor != nil){
                self.lockedSensor = nil
                self.mainState.untargetSensors()
            }
        }
    }
    
    func sensorNewSelection(_ new: CompassSensor?, _ old: CompassSensor?) {
        if new != nil && old == nil {
            //new selected
            hapticSubject.send(.Selected)
        } else if new == nil && old != nil {
            //unselected
            hapticSubject.send(.Unselected)
        }
        
        if let new = new, let old = old {
            if new.id != old.id {
                //new selected
                hapticSubject.send(.Selected)
            }
        }
    }
    
    func sensorNewLock(_ new: CompassSensor?, _ old: CompassSensor?) {
        if new != nil && old == nil {
            //new locked
            lockSensorAfterDelay()
        } else if new == nil && old != nil {
            //unselected
            cancelLocking()
        }
        
        if let new = new, let old = old {
            if new.id != old.id {
                //new selected
                lockSensorAfterDelay()
            }
        }
    }
}
